$(function() {
    var hl_unknown = "4px solid purple";

    // find unknown

    $("#btnFindUnknown").click(function(){
        var unknownMO = $("select[name=moneyOwner] option[value=unknown]:selected");
        console.log("unknown money owners #: " + unknownMO.length);

        var unknownFT = $("select[name=feeType] option[value=unknown]:selected");
        console.log("unknown fee types #: " + unknownFT.length);

        var unknownCCN = $("input[name=conversionId][value='']:visible");
        console.log("unknown conversion Charge Number #: " + unknownCCN.length);

        var unknownTI = $("input[name=transferId][value='']:visible");
        console.log("unknown transfer id #: " + unknownTI.length);

        if (unknownMO.length > 0) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(unknownMO[0]).closest("select").offset().top - 120
            }, 1000);
        } else if (unknownFT.length > 0) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(unknownFT[0]).closest("select").offset().top - 120
            }, 1000);
        } else if (unknownCCN.length > 0) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(unknownCCN[0]).closest("span").offset().top - 20
            }, 1000);        
        } else if (unknownTI.length > 0) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(unknownTI[0]).closest("span").offset().top - 20
            }, 1000);        
        } else {
            alert("Все перевод отмечены!");
        }
    });

    var last = location.href.trim().split("/").filter(x => x.length > 0).slice(-1)[0];
    var year = isNaN(last) ? "" : last
    console.log('year', year);

    // money owner

    $("select[name=moneyOwner]").on("change", function() {
        var select = $(this);
        var icon = select.next("ins");
        var val = this.value;

        if (icon.length == 0) {
            icon = $("<ins/>");
            select.after(icon);
        }

        var url = "/save-money-owner?doc="
            + $(this).data("id")
            + "&person=" + val
            + "&year=" + year

        icon.html(" ...");
        $.get(url, function(data) {
            if (data == "success") {
                icon.html(" OK");

                if (val == "unknown") {
                    select.css("border", hl_unknown);
                } else {
                    select.css("border", "");
                }
            } else {
                icon.html(" NOT SAVED!");
                alert(data);
            }
        });
    });

    $("select[name=moneyOwner]").each(function(){
        var val = $(this).data("value");
        $(this).val(val);

        if (val == "unknown") {
            $(this).css("border", hl_unknown);
        }
    });

    // fee type

    $("select[name=feeType]").on("change", function() {
        var select = $(this);
        var icon = select.next("ins");
        var docId = $(this).data("id");
        var type = this.value;

        if (icon.length == 0) {
            icon = $("<ins/>");
            select.after(icon);
        }

        var url = "/save-fee-type?doc="
            + docId
            + "&type=" + type
            + "&year=" + year

        icon.html(" ...");
        $.get(url, function(data) {
            if (data == "success") {
                icon.html(" OK");

                if (type == "unknown") {
                    select.css("border", hl_unknown);
                } else {
                    select.css("border", "");
                }
            } else {
                icon.html(" NOT SAVED!");
                alert(data);
            }
        });

        $("#conversionId_" + docId).toggle(type == "conversion" || type == "conversionFee" || type == "transfer");
        $("#transferId_" + docId).toggle(type == "transferFee");

        if (type == "olesya_credit" || type == "olesya_debit" || type == "taxes") {
            $("#moneyOwner_" + docId).val("olesya");
            $("#moneyOwner_" + docId).attr("disabled", "disabled");

            setTimeout(function(){
                $("#moneyOwner_" + docId).trigger("change");
            }, 100);
        } else {
            $("#moneyOwner_" + docId).removeAttr("disabled");
        }
    });

    $("select[name=feeType]").each(function(){
        var type = $(this).data("value");
        var docId = $(this).data("id");
        
        $(this).val(type);
        $("#conversionId_" + docId).toggle(type == "conversion" || type == "conversionFee" || type == "transfer");
        $("#transferId_" + docId).toggle(type == "transferFee");

        if (type == "unknown") {
            $(this).css("border", hl_unknown);
        }

        if (type == "olesya_credit" || type == "olesya_debit" || type == "taxes") {
            $("#moneyOwner_" + docId).attr("disabled", "disabled");
        } else {
            $("#moneyOwner_" + docId).removeAttr("disabled");
        }
    });

    $("input[name=conversionId]").each(function(){
        if (this.value == "") {
            $(this).css("border", hl_unknown);
        }
    });

    // save conversionId

    $("input[name=saveConversionId]").on("click", function(){
        var input = $(this).siblings("input[type=text]");
        var value = $(input).val();
        var docId = $(input).data("id");

        if (isNaN(value) || value == "") {
            $(input).val("");
            return;
        }

        var icon = $(this).next("ins");
        if (icon.length == 0) {
            icon = $("<ins/>");
            $(this).after(icon);
        }

        var url = "/save-conversion-id"
            + "?doc=" + docId
            + "&num=" + value
            + "&year=" + year;

        icon.html(" ...");

        $.getJSON(url, function(data) {
            if (data.success == "true") {
                icon.html(" OK");

                if (value == "") {
                    $(input).css("border", hl_unknown);
                } else {
                    $(input).css("border", "");
                }

                var moneyOwner = $(input).closest("td").find("select[name=moneyOwner]");
                var number = $(input).closest("td").find("i[name=conversionChargeNumber]");

                if (data.convMoneyOwner != "unknown") {
                    if (moneyOwner.val() == "unknown") {
                        moneyOwner.val(data.convMoneyOwner);
                        moneyOwner.trigger("change");

                        number.html("№ (" + data.convNumber + ")");
                    } else if (moneyOwner.val() != data.convMoneyOwner) {
                        alert("Внимание! У конвертации другой владелец!");
                    }
                } else {
                    number.html("№ (?)");
                }

                var color = $("#docId_" + data.convDocId)
                  .find(">td:first")
                  .css("background-color");
                  
                $("#docId_" + docId)
                  .find(">td:first")
                  .css("background-color", color);
            } else {
                icon.html(" NOT SAVED!");
                $(input).css("border", hl_unknown);
                alert(data.error);
            }
        });
    });

    $("input[name=transferId]").each(function(){
        if (this.value == "") {
            $(this).css("border", hl_unknown);
        }
    });

    // save transfer id

    $("input[name=saveTransferId]").on("click", function(){
        var input = $(this).siblings("input[type=text]");
        var value = $(input).val();
        var docId = $(input).data("id");

        if (isNaN(value) || value == "") {
            $(input).val("");
            return;
        }

        var icon = $(this).next("ins");
        if (icon.length == 0) {
            icon = $("<ins/>");
            $(this).after(icon);
        }

        var url = "/save-transfer-id"
            + "?doc=" + docId
            + "&num=" + value
            + "&year=" + year;

        icon.html(" ...");

        $.getJSON(url, function(data) {
            if (data.success == "true") {
                icon.html(" OK");

                if (value == "") {
                    $(input).css("border", hl_unknown);
                } else {
                    $(input).css("border", "");
                }

                var moneyOwner = $(input)
                  .closest("td")
                  .find("select[name=moneyOwner]");

                if (data.transMoneyOwner != "unknown") {
                    if (moneyOwner.val() == "unknown") {
                        moneyOwner.val(data.transMoneyOwner);
                        moneyOwner.trigger("change");
                    } else if (moneyOwner.val() != data.transMoneyOwner) {
                        alert("Внимание! У перевода другой владелец!");
                    }
                } 

                var color = $("#docId_" + data.transDocId)
                  .find(">td:first")
                  .css("background-color");
                  
                $("#docId_" + docId)
                  .find(">td:first")
                  .css("background-color", color);
            } else {
                icon.html(" NOT SAVED!");
                $(input).css("border", hl_unknown);
                alert(data.error);
            }
        });
    });
});

