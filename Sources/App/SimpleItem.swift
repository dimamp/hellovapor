//
//  SimpleItem.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

struct SimpleItem: ItemProtocol {
    // Тип счёта;
    //let type: String

    // Номер счета;
    let acc_num: String

    // Валюта;
    let currency: String

    // Дата операции;
    let date: String

    // Референс проводки;
    let ref: String

    // Описание операции;
    var text: String

    // Приход;
    let `in`: String

    // Расход;
    let out: String

    let id: Int
    let type: AccountType

    let isCredit: Bool

    ///
    var balance: Double? = nil
    var value: Double? {
        if let num = isCredit
            ? Double(self.in.replacingOccurrences(of: ",", with: "."))
            : Double(self.out.replacingOccurrences(of: ",", with: "."))
        {
            return num < 0 ? -num : num
        } else {
            return nil
        }
    }
    var initialValue: Double? {
        return 37.70
    }
    var dateAsDate: Date? {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd.MM.yy"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC")

        return dateFormat.date(from: date)
    }
    var sortBy: (Date, String) {
        if let date = dateAsDate {
            return (date, isCredit ? "C" : "D")
        } else {
            return (Date.init(timeIntervalSince1970: 0), isCredit ? "C" : "D")
        }

    }
    var valueAsString: String {
        return isCredit ? self.in : self.out
    }

    init(id: Int, type: AccountType, line: [Substring]) {
        self.id = id
        self.type = type

        let get: (String) -> String = {
            if let index = SimpleOrderedItems.index(of: $0), index < line.count {
                return String(line[index])
            } else {
                return ""
            }
        }

        //type = get("type")
        acc_num = get("acc_num")
        //currency = get("currency")
        date = get("date")
        ref = get("ref")
        text = get("text")
        `in` = get("in")
        out = get("out")

        isCredit = out == "0"

        switch type {
        case .dima, .olesya, .rur: currency = "₽"
        case .tns, .usd, .usdCard: currency = "$"
        }
    }
}
