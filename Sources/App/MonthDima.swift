//
//  MonthDima.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

class MonthDima: AccountMonth {
    var items: [SimpleItem]
    var rows: [ItemProtocol] {
        return items
    }

    var inputBalance: Double = 0
    var balance: Double = 0

    var input = Value()

    init(items: [SimpleItem]) {
        self.items = items

        self.items.enumerated().forEach { (offset, item) in
            if DimaTransferReason.contains(ref: item.ref) {
                self.items[offset].text = DimaTransferReason.getText(byRef: item.ref)!
            }
        }
    }

    func set(balance: Double, forRow: Int) {
        items[forRow].balance = balance
    }

    func calcInput(_ prevMonth: Month?) {
        // суммарный приход
        input.now = items.reduce(0) { (result, item) -> Double in
            return item.isCredit
                ? result + item.value!
                : result
        }
        input.prev = prevMonth?.usdCard?.input.total ?? inputBalance
        input.total = input.prev + input.now
    }
}

