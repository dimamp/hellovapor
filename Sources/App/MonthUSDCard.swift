//
//  MonthUSDCard.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

class MonthUSDCard: AccountMonth {
    var items: [SimpleItem]
    var rows: [ItemProtocol] {
        return items
    }

    var mcc = [
        "MCC6011", // снятие наличности
        "MCC5942", // Книжные магазины
        "MCC7399", // Различные бизнес-услуги.
        "MCC4215", // Услуги курьера, доставка
    ]

    var dima_skype: [String: Double] = [
        "28.02.19": 1000,
        "11.02.19": 1000,
        "20.01.19": 1000,
        "17.12.18": 2000,
        "30.11.18": 1000,
        "03.11.18": 1000,
        "14.10.18": 1000,
        "28.09.18": 1000,
        "11.09.18": 1000,
        "23.08.18": 2000,
        "31.07.18": 1000,
        "18.07.18": 1000,
        "26.06.18": 1000,
        "22.05.18": 2000,
        "30.03.18": 1000,
        "26.03.18": 1000,
        "01.03.18": 1000,
        "01.02.18": 1000,
        "12.01.18": 1000,
    ]

    var inputBalance: Double = 0
    var balance: Double = 0

    var input = Value()
    var out_to_rur = Value()
    var out_to_cash = Value()
    var out_to_payments = Value()
    var out_to_bank = Value()
    var out_to_dima = Value()

    init(items: [SimpleItem]) {
        self.items = items
    }

    func calc(prevMonth: Month?, inputBalance: Double) {
        self.inputBalance = inputBalance

        calcInput(prevMonth)
        calcOutput(prevMonth)

        // баланс
        let prev_balance = prevMonth?.usdCard?.balance ?? inputBalance
        balance = prev_balance + input.now
            - out_to_rur.now
            - out_to_cash.now
            - out_to_payments.now
            - out_to_bank.now
    }

    private func calcInput(_ prevMonth: Month?) {
        // суммарный приход
        input.now = items.reduce(0) { (result, item) -> Double in
            return item.isCredit
                ? result + item.value!
                : result
        }
        input.prev = prevMonth?.usdCard?.input.total ?? inputBalance
        input.total = input.prev + input.now
    }

    private func isTransfer(_ text: String) -> Bool {
        return text.contains("Внутрибанковский перевод между счетами с конвертацией")
    }

    func outToDima(_ item: SimpleItem) -> Bool {
        let code = mcc.first { item.text.contains($0) }
        return !item.isCredit
                && code == nil
                && isTransfer(item.text)
                && dima_skype.keys.contains(item.date)
                && dima_skype[item.date] == item.value
    }

    private func calcOutput(_ prevMonth: Month?) {
        // перевод на рублевый счет
        out_to_rur.now = items.reduce(0) { (result, item) -> Double in
            let code = self.mcc.first { item.text.contains($0) }

            return (!item.isCredit && code == nil && isTransfer(item.text))
                ? result + item.value!
                : result
        }

        out_to_rur.prev = prevMonth?.usdCard?.out_to_rur.total ?? 0
        out_to_rur.total = out_to_rur.prev + out_to_rur.now

        // диме по запросу из скайпа
        // перевод на рублевый счет
        out_to_dima.now = items.reduce(0) { (result, item) -> Double in
            return outToDima(item)
                ? result + dima_skype[item.date]!
                : result
        }

        out_to_dima.prev = prevMonth?.usdCard?.out_to_dima.total ?? 0
        out_to_dima.total = out_to_dima.prev + out_to_dima.now

        // снятие наличных
        out_to_cash.now = items.reduce(0) { (result, item) -> Double in
            let code = self.mcc.first { item.text.contains($0) }

            return (!item.isCredit && code == "MCC6011")
                ? result + item.value!
                : result
        }

        out_to_cash.prev = prevMonth?.usdCard?.out_to_cash.total ?? 0
        out_to_cash.total = out_to_cash.prev + out_to_cash.now

        // отплата по карты
        out_to_payments.now = items.reduce(0) { (result, item) -> Double in
            let code = self.mcc.first { item.text.contains($0) }

            return (!item.isCredit && code != nil && code != "MCC6011")
                ? result + item.value!
                : result
        }

        out_to_payments.prev = prevMonth?.usdCard?.out_to_payments.total ?? 0
        out_to_payments.total = out_to_payments.prev + out_to_payments.now

        // Ком.за выд.нал
        out_to_bank.now = items.reduce(0) { (result, item) -> Double in
            return (!item.isCredit && item.text.contains("Ком.за выд.нал"))
                ? result + item.value!
                : result
        }

        out_to_bank.prev = prevMonth?.usdCard?.out_to_bank.total ?? 0
        out_to_bank.total = out_to_bank.prev + out_to_bank.now

    }

    func set(balance: Double, forRow: Int) {
        items[forRow].balance = balance
    }
}
