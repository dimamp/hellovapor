//
//  Value.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

struct Value: Codable {
    var now: Double = 0
    var prev: Double = 0
    var total: Double = 0
}
