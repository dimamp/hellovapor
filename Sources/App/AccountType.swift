//
//  AccountType.swift
//  App
//
//  Created by Dima on 19.12.2019.
//

import Foundation

enum AccountType: String, Codable {
    case rur = "rur" // 40802810023070000221 рублевый счет ИП
    case usd = "usd" // 40802840723070000012 валютный счет ИП
    case tns = "tns" // 40802840023070000013 транзитный счет ИП
    case usdCard = "usd_card" // 40817840408390001145 долларовая карта Олеси
    case dima = "dima" // 40817810508390016701 Рублевая карта Димы
    case olesya = "olesya" // 40817810209620014264 Рублевая карта Олеси
}
