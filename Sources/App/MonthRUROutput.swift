//
//  MonthRUROutput.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

struct MonthRUROutput: Codable {
    let dimaCard = "40817810508390016701" // Рублевая карта Димы
    let olesCard = "40817810209620014264" // Рублевая карта Олеси

    // перевод Диме (сумма в рублях)
    var dima_rur = Value()

    // перевод Олесе (сумма в рублях)
    var olesya_rur = Value()

    var eng = Value()
    var elba = Value()
    var tax = Value()
    var olesya_debit = Value()

    var conversions: [USDConversion]? = nil

    var transferComissionsByMonth: [Int: Double] = [:]

    mutating func calc(
        _ month: MonthRUR,
        prevMonth: Month?,
        nextMonth: Month?
    ) {
        conversions = month.items.compactMap { (item) -> USDConversion? in
            if (item.feeType == .conversion || item.feeType == .olesya_credit)
                && item.moneyOwner != .unknown {
                return USDConversion(conversion: item, to: item.moneyOwner)
            } else {
                return nil
            }
        }

        var allItems = month.items

        if let prev = prevMonth?.rur {
            allItems += prev.items
        }

        if let next = nextMonth?.rur {
            allItems += next.items
        }

        conversions!.enumerated().forEach { (i) in
            let conv = i.element.conversion!

            conversions![i.offset].conversionComission = allItems.filter({
                (item) -> Bool in item.feeType == .conversionFee
                    //&& item.moneyOwner == conv.moneyOwner
                    && item.conversionId == conv.id
            })

            conversions![i.offset].transfer = allItems.filter({
                (item) -> Bool in item.feeType == .transfer
                    //&& item.moneyOwner == conv.moneyOwner
                    && item.conversionId == conv.id
            })

            conversions![i.offset].transferComissions = allItems.filter({
                (item) -> Bool in item.feeType == .transferFee
                    //&& item.moneyOwner == conv.moneyOwner
                    && conversions![i.offset].transfer.contains(where: { (t) -> Bool in
                        t.id == item.transferId
                    })
            })
        }

        // подсвечиваем найденые элементы

        month.items.enumerated().forEach { (item) in
            if let found = conversions?.first(where: { (c) -> Bool in
                c.contains(item.element.id)
            }) {
                month.items[item.offset].cssStyle = "background-color: \(found.color);"
            }
            else if item.element.feeType == .serviceFee {
                month.items[item.offset].cssStyle = "background-color: white;"
            }
            else if let found = prevMonth?.rur?.out.conversions?.first(where: { (c) -> Bool in
                c.contains(item.element.id)
            }) {
                month.items[item.offset].cssStyle = "background-color: \(found.color);"
            }
        }

        //
        let inputBalance = 0.0

        //
        dima_rur.now = month.items.reduce(0) { (result, item) -> Double in
            if item.moneyOwner == .dima {
                if item.feeType == .transfer
                    || item.feeType == .transferFee
                    || item.feeType == .conversionFee
                    || item.feeType == .serviceFee
                {
                    return result + item.value!
                }
            }
            return result
        }

        dima_rur.prev = prevMonth?.rur?.out.dima_rur.total ?? inputBalance
        dima_rur.total = dima_rur.now + dima_rur.prev

        //
        olesya_rur.now = month.items.reduce(0) { (result, item) -> Double in
            if item.moneyOwner == .olesya {
                if item.feeType == .transfer
                    || item.feeType == .transferFee
                    || item.feeType == .conversionFee
                    || item.feeType == .serviceFee
                {
                    return result + item.value!
                }
            }
            return result
        }

        olesya_rur.prev = prevMonth?.rur?.out.olesya_rur.total ?? inputBalance
        olesya_rur.total = olesya_rur.now + olesya_rur.prev

        let individualFees: [FeeType] = [
            .transfer,
            .transferFee,
            .english,
            .elba,
            .taxes,
            .olesya_credit,
            .olesya_debit,
        ]

        //
        month.items.forEach { (item) in
            if item.moneyOwner == .both {
                if individualFees.contains(item.feeType) {
                    //print("!! ACHTUNG, item id:\(item.id) has type \(item.feeType)")
                }

                switch item.feeType {
                case .serviceFee:
                    dima_rur.now += item.value! / 2
                    dima_rur.total = dima_rur.now + dima_rur.prev

                    olesya_rur.now += item.value! / 2
                    olesya_rur.total = olesya_rur.now + olesya_rur.prev

                case .transfer, .conversionFee:
                    if let c = findConversion(in: month.items, byItem: item) {
                        divide(item: item, conv: c)
                    }

                case .transferFee:
                    if let t = findTransfer(in: allItems, byItem: item) {
                        if let c = findConversion(in: allItems, byItem: t) {
                            divide(item: item, conv: c)
                        }
                    }

                case .conversion:
                    print("! DOUBLE CHECK shared conversion id:\(item.id)")

                case .olesya_credit, .unknown, .english, .elba, .olesya_debit, .taxes:
                    break;
                }
            }
        }

        //

        let english = month.items.filter { (item) -> Bool in
            item.feeType == .english
        }

        let englishTransferFees = month.items.filter { (item) -> Bool in
            item.feeType == .transferFee && english.contains(where: { (eng) -> Bool in
                eng.id == item.transferId
            })
        }

        eng.now = english.reduce(0) {
            (result, item) -> Double in result + item.value!
        }

        eng.prev = prevMonth?.rur?.out.eng.total ?? inputBalance
        eng.total = eng.now + eng.prev

        let color = randomColor()
        month.items.enumerated().forEach { (item) in
            if english.contains(where: { (eng) -> Bool in
                eng.id == month.items[item.offset].id
            }) {
                month.items[item.offset].cssStyle = "background-color: \(color);"
            }
            else if englishTransferFees.contains(where: { (fee) -> Bool in
                fee.id == month.items[item.offset].id
            }) {
                month.items[item.offset].cssStyle = "background-color: \(color);"
            }
        }

        month.items.enumerated().forEach { (item) in
            if let conv = conversions?.first(where: { (cnv) -> Bool in
                cnv.conversion?.id == item.element.id
            }) {
                let ids = conv.getItemIds().filter({ (i) -> Bool in
                    i != conv.conversion?.id
                })

                month.items[item.offset].spent = allItems.reduce(0, {
                    (result, item) -> Double in
                    let value = ids.contains(item.id)
                        ? result + (item.value ?? 0)
                        : result
                    return value
                })
            }
        }

        //

        elba.now = month.items.reduce(0) {
            (result, item) -> Double in
            return item.feeType == .elba
                ? result + item.value!
                : result
        }

        elba.prev = prevMonth?.rur?.out.elba.total ?? inputBalance
        elba.total = elba.now + elba.prev

        //

        tax.now = month.items.reduce(0) {
            (result, item) -> Double in
            return item.feeType == .taxes
                ? result + item.value!
                : result
        }

        tax.prev = prevMonth?.rur?.out.tax.total ?? inputBalance
        tax.total = tax.now + tax.prev

        //

        olesya_debit.now = month.items.reduce(0) {
            (result, item) -> Double in
            return item.feeType == .olesya_debit
                ? result + item.value!
                : result
        }

        olesya_debit.prev = prevMonth?.rur?.out.olesya_debit.total ?? inputBalance
        olesya_debit.total = olesya_debit.now + olesya_debit.prev
    }

    mutating private func divide(item: Item, conv: Item) {
        var usd = conv.value! / conv.getExchangeRate()!
        usd = Double(String(format: "%.2f", usd))!

        let kcs = knownConversions.filter({ (kc) -> Bool in
            kc.date == conv.dateAsDate && kc.sum <= usd
        })

        if kcs.count != 1 {
            print("!!!-2 Общая конвертацию id=\(conv.id) на сумму \(usd)")
        } else {
            let dimaPart = kcs.first!.to == .dima
                ? kcs.first!.sum
                : usd - kcs.first!.sum

            let olesyaPart = usd - dimaPart

            dima_rur.now += item.value! / usd * dimaPart
            dima_rur.total = dima_rur.now + dima_rur.prev

            olesya_rur.now += item.value! / usd * olesyaPart
            olesya_rur.total = olesya_rur.now + olesya_rur.prev
        }
    }

    private func findConversion(in items: [Item], byItem item: Item) -> Item? {
        guard let n = item.conversionId else {
            print("Внимание: транзакция id:\(item.id) не связана с конвертацией")
            return nil
        }

        let converstions = items.filter({ (i) -> Bool in
            i.feeType == .conversion
                && i.id == n
                && i.year! <= item.year!
                && i.month! <= item.month!
        })

        if converstions.count == 0 {
            print("Внимание: перевод id:\(item.id) связан с несуществующей конвертацией")
        }
        else if converstions.count > 1 {
            print("Внимание: перевод id:\(item.id) связан с несколькими конвертациями")
        }
        else {
            return converstions.first!
        }

        return nil
    }

    private func findTransfer(in items: [Item], byItem item: Item) -> Item? {
        guard let n = item.transferId else {
            print("Внимание: комиссия за перевод id:\(item.id) не связана с переводом")
            return nil
        }

        let transfers = items.filter({ (i) -> Bool in
            i.feeType == .transfer && i.id == n
        })

        if transfers.count == 0 {
            print("Внимание: комиссия за перевод id:\(item.id) связана с несуществующим переводом")
        }
        else if transfers.count > 1 {
            print("Внимание: комиссия за перевод id:\(item.id) связана с несколькими переводами")
        }
        else {
            return transfers.first!
        }

        return nil
    }

    // находим элементы комисси за внутренние переводы
    private func findTransferComissions(items: [Item]) -> [Int: Double] {
        let transferComissions = items.filter({ (item) -> Bool in
            !item.isCredit
                && item.text.contains("Комиссия за внутренние переводы в валюте РФ на счета ФЛ")
        })

        let monthAbbr = [
            "ЯНВ": 1,
            "ФЕВ": 2,
            "МАР": 3,
            "АПР": 4,
            "МАЙ": 5,
            "ИЮН": 6,
            "ИЮЛ": 7,
            "АВГ": 8,
            "СЕН": 9,
            "ОКТ": 10,
            "НОЯ": 11,
            "ДЕК": 12
        ]

        var transferComissionsByMonth = [Int: Double]()

        for tc in transferComissions {
            if let range = tc.text.range(
                of: "\\d{2}\\w{3}\\d{2}",
                options: .regularExpression
            ) {
                let text = String(tc.text[range])
                let monthStart = text.index(text.startIndex, offsetBy: 2)
                let monthEnd = text.index(text.endIndex, offsetBy: -2)
                let range = monthStart..<monthEnd
                let monthName = String(text[range])
                let month = monthAbbr[monthName]!

                if !transferComissionsByMonth.keys.contains(month) {
                    transferComissionsByMonth[month] = 0
                }

                transferComissionsByMonth[month]! += tc.value!
            }
        }

        return transferComissionsByMonth
    }
}
