//
//  ItemProtocol.swift
//  App
//
//  Created by Dima on 19.12.2019.
//

import Foundation

protocol ItemProtocol: Codable {
    init(id: Int, type: AccountType, line: [Substring])

    var value: Double? {get}
    var initialValue: Double? {get}
    var isCredit: Bool {get}
    var balance: Double? {get set}
    var dateAsDate: Date? {get}
    var sortBy: (Date, String) {get}
    var text: String {get set}
    var valueAsString: String {get}
    var type: AccountType {get}
    var currency: String {get}
}
