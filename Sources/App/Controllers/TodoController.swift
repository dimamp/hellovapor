import Vapor
import Leaf
import HTTP

/// Controls basic CRUD operations on `Todo`s.
final class TodoController {
    static let EOL: Character = "\n"
    static let EOI: Character = "\t"

    let dbConnectionProperties = MyCouchDB.ConnectionProperties(
        secure: true,
        hostname: "c3e79186-40a7-4665-add7-41eb3a5bd08f-bluemix"
            + ".cloudantnosqldb.appdomain.cloud"
    )

    func index(_ req: Request) throws -> Future<View> {
        let db = MyCouchDB(
            connectionProperties: dbConnectionProperties,
            worker: req
        )

        let futResp: EventLoopPromise<View> = req.eventLoop.newPromise()

        let client = db.connect(timeout: 30)
        client.catch { (error) in
            futResp.fail(error: error)
        }
        
        let year = 2019
        let host = "localhost"
        let port = 8080

        let request = client.flatMap { (client) -> Future<HTTPResponse> in
            client.send(db.getAllDocs(database: "rur_\(year)"))
        }
        .flatMap { (response) -> EventLoopFuture<AllDocsResponse?> in
            guard let data = response.body.data else {
                return req.future(nil)
            }

            let decoder = JSONDecoder()
            let response = try decoder.decode(AllDocsResponse.self, from: data)

            return req.future(response)
        }
        .and(loadCSV(year: 2015, type: .usd, host: host, port: port, worker: req))
        .and(loadCSV(year: 2016, type: .usd, host: host, port: port, worker: req))
        .and(loadCSV(year: 2017, type: .usd, host: host, port: port, worker: req))
        .and(loadCSV(year: 2018, type: .usd, host: host, port: port, worker: req))
        .and(loadCSV(year: 2019, type: .usd, host: host, port: port, worker: req))
        .and(loadCSV(year: 2015, type: .tns, host: host, port: port, worker: req))
        .and(loadCSV(year: 2016, type: .tns, host: host, port: port, worker: req))
        .and(loadCSV(year: 2017, type: .tns, host: host, port: port, worker: req))
        .and(loadCSV(year: 2018, type: .tns, host: host, port: port, worker: req))
        .and(loadCSV(year: 2019, type: .tns, host: host, port: port, worker: req))

        request.whenFailure { (error) in
            futResp.fail(error: error)
        }
        
        request.whenSuccess { (arg0) in
            let response = arg0.0.0.0.0.0.0.0.0.0.0
            let usd2015 = arg0.0.0.0.0.0.0.0.0.0.1
            let usd2016 = arg0.0.0.0.0.0.0.0.0.1
            let usd2017 = arg0.0.0.0.0.0.0.0.1
            let usd2018 = arg0.0.0.0.0.0.0.1
            let usd2019 = arg0.0.0.0.0.0.1
            let tns2015 = arg0.0.0.0.0.1
            let tns2016 = arg0.0.0.0.1
            let tns2017 = arg0.0.0.1
            let tns2018 = arg0.0.1
            let tns2019 = arg0.1
            
            let usdItems: [Item] = self.getItems(
                data: [
                    usd2015.body.data,
                    usd2016.body.data,
                    usd2017.body.data,
                    usd2018.body.data,
                    usd2019.body.data,
                ],
                type: .usd,
                separator: TodoController.EOI
            )
            
            let tnsItems: [Item] = self.getItems(
                data: [
                    tns2015.body.data,
                    tns2016.body.data,
                    tns2017.body.data,
                    tns2018.body.data,
                    tns2019.body.data,
                ],
                type: .tns,
                separator: TodoController.EOI
            )
                                    
            let rurItems = response?.rows.compactMap({
                doc -> Item? in doc.doc
            }) ?? []
            
            let rurMonths = self.groupItemsByMonth(rurItems, type: .rur)
            let usdMonths = self.groupItemsByMonth(usdItems, type: .usd)
            let tnsMonths = self.groupItemsByMonth(tnsItems, type: .tns)
            
            rurMonths.enumerated().forEach { (item) in
                if let usdMonth = usdMonths.first(where: { (usd) -> Bool in
                    usd == item.element
                }) {
                    rurMonths[item.offset].usd = usdMonth.usd
                }

                if let tnsMonth = tnsMonths.first(where: { (tns) -> Bool in
                    tns == item.element
                }) {
                    rurMonths[item.offset].tns = tnsMonth.tns
                }
            }
            
            let inputBalance = rurMonths
                .first?.byType[.rur]!!.rows.first?.initialValue ?? 0
            
            self.calc(rurMonths, type: .rur, inputBalance: inputBalance)

            var transferComissionsByMonth: [Int: Double] = [:]

            rurMonths.forEach { (month) in
                month.rur?.out.transferComissionsByMonth.forEach({ (comission) in
                    if !transferComissionsByMonth.keys.contains(comission.key) {
                        transferComissionsByMonth[comission.key] = 0
                    }
                    transferComissionsByMonth[comission.key]! += comission.value
                })
            }

            print(transferComissionsByMonth)

                        
            do {
                let renderer = try req.view().render(
                    "hello",
                    [
                        "groupedItems": rurMonths
                    ]
                )
                
                renderer.whenSuccess { (view) in
                    futResp.succeed(result: view)
                }
                
                renderer.whenFailure { (error) in
                    futResp.fail(error: error)
                }                
            } catch let error {
                futResp.fail(error: error)
            }
        }

        return futResp.futureResult
    }

    func groupItemsByMonth(_ items: [ItemProtocol], type: AccountType) -> [Month] {
        let filtered = filterItems(items)

        let monthFormat = DateFormatter()
        monthFormat.dateFormat = "yyyy|MM"
        monthFormat.timeZone = TimeZone(abbreviation: "UTC")

        var result: [String: [ItemProtocol]] = Dictionary.init(grouping: filtered) {
            if let date = $0.dateAsDate {
                return monthFormat.string(from: date)
            } else {
                return "Unknown"
            }
        }

        result.removeValue(forKey: "Unknown")
        
        let months: [Month] = result.map { (item) -> Month in
            let m = Month(
                month: Int(item.key.split(separator: "|").last!)!,
                year: Int(item.key.split(separator: "|").first!)!
            )

            let monthItems = item.value.sorted(by: {
                return $0.sortBy < $1.sortBy
            })

            m.createIfEmpty(type: type, items: monthItems)

            return m
        }
        
        let sorted = months.sorted(by: { return $0 < $1 })

        let balance = sorted
            .first!
            .byType[type]!!
            .rows
            .first!
            .initialValue!

        return calcBalance(months: sorted, type: type, initialBalance: balance)
    }
    
    func filterItems(_ items: [ItemProtocol]) -> [ItemProtocol] {
        if items.count == 0 {
            return []
        }

        print("Начальное количество записей: \(items.count)")

        // Удаляем операции с нулевой суммой
        var filtered = items.filter { (item) -> Bool in (item.value ?? 0) > 0 }

        print("После удаления с нулевой суммой: \(filtered.count)")

        // Удаляем операции типа Переоценка - курсовая разница
        filtered = filtered.filter({ (item) -> Bool in
            !item.text.contains("Переоценка") && !item.text.contains("Дооценка")
        })

        print("После удаления с типом переоценка курсовой разницы: \(filtered.count)")

        //

    //    if items is [SimpleItem] {
    //        filtered = filtered.filter({ (item) -> Bool in
    //            // нас интересуют только две эти операции
    //            item.text.contains("Внутрибанковский перевод между счетами")
    //                || item.text.contains("MCC6011") // снятие наличности
    //        })
    //    }

        return filtered.sorted(by: { return $0.dateAsDate! < $1.dateAsDate! })
    }
    
    private func calcBalance(months: [Month], type: AccountType, initialBalance: Double) -> [Month] {
        var balance = initialBalance

        months.enumerated().forEach { (month) in
            month.element.byType[type]??.rows.enumerated().forEach({ (item) in
                let val = item.element.value!

                balance = (item.element.isCredit)
                    ? balance + val
                    : balance - val

                months[month.offset]
                    .byType[type]!!
                    .set(balance: balance, forRow: item.offset)
            })
        }
        return months
    }
    
    
    private func getItems<T: ItemProtocol>(
        data: [Data?],
        type: AccountType,
        separator: Character,
        eol: Character = TodoController.EOL
    ) -> [T] {
        var lines = [String.SubSequence]()
        
        for dataItem in data.compactMap({ $0 }) {
            if let text = String(
                data: dataItem,
                encoding: String.Encoding.windowsCP1251
            ) {
                lines += text.split(separator: eol)
            }
        }
        
        let items = lines.enumerated().map {
            T(
                id: $0.offset + 1,
                type: type,
                line: $0.element.split(
                    separator: separator,
                    maxSplits: Int.max,
                    omittingEmptySubsequences: false
                )
            )
        }

        return items
    }

    func loadCSV(
        year: Int,
        type: AccountType,
        host: String,
        port: Int,
        worker: Worker
    ) -> EventLoopFuture<HTTPResponse> {
        let client = HTTPClient.connect(
            scheme: .http,
            hostname: host,
            port: port,
            connectTimeout: TimeAmount.seconds(10),
            on: worker,
            onError: { error in
                let f: () -> EventLoopFuture<HTTPClient> = {
                    return worker.future(error: error)
                }
                _ = f()
            }
        ).flatMap {
            (client) -> EventLoopFuture<HTTPResponse> in
            let usdFilename = "\(year)_" + type.rawValue + ".csv"
            return client.send(HTTPRequest(
                method: .GET,
                url: "/\(usdFilename)"
            ))
        }
        
        return client
    }
    
    private func calc(_ data: [Month], type: AccountType, inputBalance: Double) {
        data.enumerated().forEach { (month) in
            let prev: Month? = month.offset > 0
                ? data[month.offset - 1]
                : nil

            let next: Month? = month.offset + 1 < data.count
                ? data[month.offset + 1]
                : nil

            switch type {
                case .rur: month.element.rur?.calc(
                    inputBalance: inputBalance,
                    prevMonth: prev,
                    nextMonth: next
                )
                case .tns: month.element.tns?.calc(
                    prevMonth: prev,
                    inputBalance: inputBalance
                )
                case .usd: month.element.usd?.calc(
                    prevMonth: prev,
                    inputBalance: inputBalance
                )
                case .usdCard: month.element.usdCard?.calc(
                    prevMonth: prev,
                    inputBalance: inputBalance
                )
                case .dima: month.element.dima?.calcInput(prev)
                case .olesya: month.element.olesya?.calcInput(prev)
            }
        }
    }
    
    private func loadDocument(
        dbName: String,
        id: String,
        worker: Worker
    ) -> EventLoopFuture<Item?> {
        let db = MyCouchDB(
            connectionProperties: dbConnectionProperties,
            worker: worker
        )
        
        let client = db.connect(timeout: 30)
        
        let request = client.flatMap { (client) -> Future<HTTPResponse> in
            client.send(db.getDoc(database: dbName, docId: id))
        }
        .flatMap { (response) -> EventLoopFuture<Item?> in
            guard let data = response.body.data else {
                return worker.future(nil)
            }

            let decoder = JSONDecoder()
            let response = try decoder.decode(Item.self, from: data)
                
            return worker.future(response)
        }
        
        return request
    }
 
    func saveFeeType(_ req: Request) throws -> Future<String> {
        guard
            let id: String = req.query["doc"],
            let type: String = req.query["type"],
            let feeType = FeeType(rawValue: type)
        else {
            return Response(http: .init(status: .badRequest), using: req)
                .future("")
        }
        
        let futResp: EventLoopPromise<String> = req.eventLoop.newPromise()
        let dbName = "rur_2019"
        let loadDoc = loadDocument(dbName: dbName, id: id, worker: req)
        
        loadDoc.whenFailure { (error) in
            futResp.fail(error: error)
        }
        
        loadDoc.whenSuccess { (item) in
            guard var doc = item else {
                futResp.fail(error: NSError(
                    domain: "document not found",
                    code: 1,
                    userInfo: nil
                ))
                return
            }
            
            let oldFeeType = doc.feeType
            doc.feeType = feeType
            
            let db = MyCouchDB(
                connectionProperties: self.dbConnectionProperties,
                worker: req
            )
            let client = db.connect(timeout: 30)
            
            client.catch { (error) in
                futResp.fail(error: error)
            }
                
            let request = client.flatMap { (client) -> Future<HTTPResponse> in
                client.send(
                    try db.update(
                        database: dbName,
                        doc: doc
                    )
                )
            }
            .flatMap { (response) -> EventLoopFuture<OnUpdateResponse?> in
                guard let data = response.body.data else {
                    return req.future(nil)
                }
                
                _ = self.logChanges(worker: req, [
                    LogItemChange(
                        field: "feeType",
                        old: oldFeeType.rawValue,
                        new: doc.feeType.rawValue
                    )
                ])

                let decoder = JSONDecoder()
                let response = try decoder.decode(
                    OnUpdateResponse.self,
                    from: data
                )
                    
                return req.future(response)
            }
            
            request.whenFailure { (error) in
                futResp.fail(error: error)
            }

            request.whenSuccess { (result) in
                if let result = result, result.ok {
                    futResp.succeed(result: "success")
                } else {
                    futResp.fail(error: NSError(
                        domain: "document not updated",
                        code: 1,
                        userInfo: nil
                    ))
                }
            }
        }

        return futResp.futureResult
    }
    
    func saveMoneyOwner(_ req: Request) throws -> Future<String> {
        guard
            let id: String = req.query["doc"],
            let name: String = req.query["person"],
            let person = Person(rawValue: name)
        else {
            return Response(http: .init(status: .badRequest), using: req)
                .future("")
        }
        
        let futResp: EventLoopPromise<String> = req.eventLoop.newPromise()
        let dbName = "rur_2019"
        let loadDoc = loadDocument(dbName: dbName, id: id, worker: req)
        
        loadDoc.whenFailure { (error) in
            futResp.fail(error: error)
        }
        
        loadDoc.whenSuccess { (item) in
            guard var doc = item else {
                futResp.fail(error: NSError(
                    domain: "document not found",
                    code: 1,
                    userInfo: nil
                ))
                return
            }
            
            let oldMoneyOwner = doc.moneyOwner
            doc.moneyOwner = person
            
            let db = MyCouchDB(
                connectionProperties: self.dbConnectionProperties,
                worker: req
            )
            let client = db.connect(timeout: 30)
            
            client.catch { (error) in
                futResp.fail(error: error)
            }
                
            let request = client.flatMap { (client) -> Future<HTTPResponse> in
                client.send(
                    try db.update(
                        database: dbName,
                        doc: doc
                    )
                )
            }
            .flatMap { (response) -> EventLoopFuture<OnUpdateResponse?> in
                guard let data = response.body.data else {
                    return req.future(nil)
                }
                
                _ = self.logChanges(worker: req, [
                    LogItemChange(
                        field: "moneyOwner",
                        old: oldMoneyOwner.rawValue,
                        new: doc.moneyOwner.rawValue
                    )
                ])

                let decoder = JSONDecoder()
                let response = try decoder.decode(
                    OnUpdateResponse.self,
                    from: data
                )
                    
                return req.future(response)
            }
            
            request.whenFailure { (error) in
                futResp.fail(error: error)
            }

            request.whenSuccess { (result) in
                if let result = result, result.ok {
                    futResp.succeed(result: "success")
                } else {
                    futResp.fail(error: NSError(
                        domain: "document not updated",
                        code: 1,
                        userInfo: nil
                    ))
                }
            }
        }

        return futResp.futureResult
    }
    
    func saveConversionId(_ req: Request) throws -> Future<[String: String]> {
        guard
            let id: String = req.query["doc"]
        else {
            return Response(http: .init(status: .badRequest), using: req)
                .future(["error": "doc id is required"])
        }
        
        let convId: Int = req.query["num"] ?? 0
        
        let futResp: EventLoopPromise<[String: String]> = req.eventLoop.newPromise()
        let dbName = "rur_2019"
        let loadDoc = loadDocument(dbName: dbName, id: id, worker: req)
        
        loadDoc.whenFailure { (error) in
            futResp.fail(error: error)
        }
        
        loadDoc.whenSuccess { (item) in
            guard let doc = item else {
                futResp.fail(error: NSError(
                    domain: "document not found",
                    code: 1,
                    userInfo: nil
                ))
                return
            }
            
            if convId == 0 {
                self.link(
                    conversion: nil,
                    document: doc,
                    dbName: dbName,
                    worker: req
                ) { (result) in
                    futResp.succeed(result: result)
                }
            } else {
                let db = MyCouchDB(
                    connectionProperties: self.dbConnectionProperties,
                    worker: req
                )
                let client = db.connect(timeout: 30)
                    
                client.catch { (error) in
                    futResp.fail(error: error)
                }
                        
                let request = client.flatMap { (client) -> Future<HTTPResponse> in
                    client.send(
                        try db.find(
                            database: dbName,
                            selector: ["id": convId]
                        )
                    )
                }
                .flatMap { (response) -> EventLoopFuture<MyCouchDBFindResponse?> in
                    guard let data = response.body.data else {
                        return req.future(nil)
                    }

                    let decoder = JSONDecoder()
                    let response = try decoder.decode(
                        MyCouchDBFindResponse.self,
                        from: data
                    )
                        
                    return req.future(response)
                }
                
                request.whenFailure { (error) in
                    futResp.fail(error: error)
                }

                request.whenSuccess { (result) in
                    if let conv = result?.docs.first {
                        let conversions: [FeeType] = [
                            .conversion,
                            .olesya_credit,
                        ]

                        if !conversions.contains(conv.feeType) {
                            futResp.succeed(result: [
                                "success": "false",
                                "error": "Указана не конвертация",
                            ])
                        } else {
                            self.link(
                                conversion: conv,
                                document: doc,
                                dbName: dbName,
                                worker: req
                            ) { (result) in
                                futResp.succeed(result: result)
                            }
                        }                    
                    } else {
                        futResp.fail(error: NSError(
                            domain: "document not updated",
                            code: 1,
                            userInfo: nil
                        ))
                    }
                }
            }
        }

        return futResp.futureResult
    }
    
    fileprivate func link(
        conversion conv: Item?,
        document: Item,
        dbName: String,
        worker: Worker,
        callback: @escaping ([String: String])->Void
    ) {
        let convId = conv?.id ?? 0
        let convNum = conv?.conversionChargeNumber ?? ""
        let moneyOwner = conv?.moneyOwner.rawValue ?? Person.unknown.rawValue
        let convDocId = conv?._id ?? ""
        var document = document
        
        let changes: [LogItemChange] = [
            LogItemChange(
                field: "conversionId",
                old: "\(document.conversionId ?? 0)",
                new: "\(convId)"
            ),
            LogItemChange(
                field: "conversionChargeNumber",
                old: document.conversionChargeNumber ?? "",
                new: convNum
            )
        ]
        
        document.conversionId = convId
        document.conversionChargeNumber = convNum
        
        let db = MyCouchDB(
            connectionProperties: dbConnectionProperties,
            worker: worker
        )
        
        let client = db.connect(timeout: 30)
        let request = client.flatMap { (client) -> Future<HTTPResponse> in
            client.send(
                try db.update(
                    database: dbName,
                    doc: document
                )
            )
        }
        .flatMap { (response) -> EventLoopFuture<OnUpdateResponse?> in
            guard let data = response.body.data else {
                return worker.future(nil)
            }
            
            _ = self.logChanges(worker: worker, changes)

            let decoder = JSONDecoder()
            let response = try decoder.decode(
                OnUpdateResponse.self,
                from: data
            )
                
            return worker.future(response)
        }
        
        request.whenFailure { (error) in
            callback([
                "success": "false",
                "error": "\(error)",
            ])
        }
        
        request.whenSuccess { (response) in
            callback([
                "success": "true",
                "convId": "\(convId)",
                "convDocId": convDocId,
                "convNumber": convNum,
                "convMoneyOwner": moneyOwner,
            ])
        }
    }
    
    // - - - - - -
    
    func saveTransferId(_ req: Request) throws -> Future<[String: String]> {
        guard
            let docId: String = req.query["doc"]
        else {
            return Response(http: .init(status: .badRequest), using: req)
                .future(["error": "doc id is required"])
        }
        
        let transferId: Int = req.query["num"] ?? 0
        
        let futResp: EventLoopPromise<[String: String]> = req.eventLoop.newPromise()
        let dbName = "rur_2019"
        let loadDoc = loadDocument(dbName: dbName, id: docId, worker: req)
        
        loadDoc.whenFailure { (error) in
            futResp.fail(error: error)
        }
        
        loadDoc.whenSuccess { (item) in
            guard let doc = item else {
                futResp.fail(error: NSError(
                    domain: "document not found",
                    code: 1,
                    userInfo: nil
                ))
                return
            }
            
            if transferId == 0 {
                futResp.succeed(result: [
                    "success": "false",
                    "error": "Перевод не указан",
                ])
            } else {
                let db = MyCouchDB(
                    connectionProperties: self.dbConnectionProperties,
                    worker: req
                )
                let client = db.connect(timeout: 30)
                    
                client.catch { (error) in
                    futResp.fail(error: error)
                }
                        
                let request = client.flatMap { (client) -> Future<HTTPResponse> in
                    client.send(
                        try db.find(
                            database: dbName,
                            selector: ["id": transferId]
                        )
                    )
                }
                .flatMap { (response) -> EventLoopFuture<MyCouchDBFindResponse?> in
                    guard let data = response.body.data else {
                        return req.future(nil)
                    }

                    let decoder = JSONDecoder()
                    let response = try decoder.decode(
                        MyCouchDBFindResponse.self,
                        from: data
                    )
                        
                    return req.future(response)
                }
                
                request.whenFailure { (error) in
                    futResp.fail(error: error)
                }

                request.whenSuccess { (result) in
                    if let trans = result?.docs.first {
                        let transfers: [FeeType] = [
                            .transfer,
                            .english,
                            .elba,
                            .taxes,
                            .olesya_debit
                        ]


                        if !transfers.contains(trans.feeType) {
                            futResp.succeed(result: [
                                "success": "false",
                                "error": "Указан не перевод",
                            ])
                        } else {
                            self.link(
                                transfer: trans,
                                document: doc,
                                dbName: dbName,
                                worker: req
                            ) { (result) in
                                futResp.succeed(result: result)
                            }
                        }
                    } else {
                        futResp.fail(error: NSError(
                            domain: "document not updated",
                            code: 1,
                            userInfo: nil
                        ))
                    }
                }
            }
        }

        return futResp.futureResult
    }
    
    fileprivate func link(
        transfer: Item,
        document: Item,
        dbName: String,
        worker: Worker,
        callback: @escaping ([String: String])->Void
    ) {
        let transId = transfer.id
        let transMoneyOwner = transfer.moneyOwner.rawValue
        let transDocId = transfer._id ?? ""
        
        let changes: [LogItemChange] = [
            LogItemChange(
                field: "transferId",
                old: "\(document.transferId ?? 0)",
                new: "\(transId)"
            )
        ]

        var document = document
        document.transferId = transId
        
        let db = MyCouchDB(
            connectionProperties: dbConnectionProperties,
            worker: worker
        )
        
        let client = db.connect(timeout: 30)
        let request = client.flatMap { (client) -> Future<HTTPResponse> in
            client.send(
                try db.update(
                    database: dbName,
                    doc: document
                )
            )
        }
        .flatMap { (response) -> EventLoopFuture<OnUpdateResponse?> in
            guard let data = response.body.data else {
                return worker.future(nil)
            }
            
            _ = self.logChanges(worker: worker, changes)

            let decoder = JSONDecoder()
            let response = try decoder.decode(
                OnUpdateResponse.self,
                from: data
            )
                
            return worker.future(response)
        }
        
        request.whenFailure { (error) in
            callback([
                "success": "false",
                "error": "\(error)",
            ])
        }
        
        request.whenSuccess { (response) in
            callback([
                "success": "true",
                "transDocId": transDocId,
                "transferId": "\(transId)",
                "transMoneyOwner": transMoneyOwner,
            ])
        }
    }
    
    private func logChanges(
        worker: Worker,
        _ changes: [LogItemChange]
    ) -> EventLoopFuture<OnUpdateResponse?> {
        let db = MyCouchDB(
            connectionProperties: MyCouchDB.ConnectionProperties(
                secure: true,
                hostname: "c3e79186-40a7-4665-add7-41eb3a5bd08f-bluemix"
                + ".cloudantnosqldb.appdomain.cloud",
                username: "c3e79186-40a7-4665-add7-41eb3a5bd08f-bluemix",
                password: "8e978ca27670896e9b3c454b4e9388fc65aa52397f17bae1e95db0c76b1c0ce8"
            ),
            worker: worker
        )
        
        let client = db.connect(timeout: 30)
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(identifier: "Asia/Bangkok")
        dateFormat.dateFormat = "dd-MM-yyyy HH:mm"
        
        let log = LogItem(
            datetime: dateFormat.string(from: Date()),
            changes: changes,
            user: "test"
        )
        
        let request = client.flatMap { (client) -> Future<HTTPResponse> in
            client.send(
                try db.log(dbName: "rur2019_log", doc: log)
            )
        }
        .flatMap { (response) -> EventLoopFuture<OnUpdateResponse?> in
            guard let data = response.body.data else {
                return worker.future(nil)
            }

            let decoder = JSONDecoder()
            let response = try decoder.decode(OnUpdateResponse.self, from: data)
                
            return worker.future(response)
        }
        
        request.whenFailure { (error) in
            print("log", error)
        }
        
        request.whenSuccess { (response) in
            print("log", response)
        }
        
        return request
    }
    
    func log(_ req: Request) throws -> Future<View> {
        let db = MyCouchDB(
            connectionProperties: dbConnectionProperties,
            worker: req
        )

        let futResp: EventLoopPromise<View> = req.eventLoop.newPromise()

        let client = db.connect(timeout: 30)
        client.catch { (error) in
            futResp.fail(error: error)
        }
        
        let year = 2019
        let host = "localhost"
        let port = 8080

        let request = client.flatMap { (client) -> Future<HTTPResponse> in
            client.send(db.getAllDocs(database: "rur2019_log"))
        }
        .flatMap { (response) -> EventLoopFuture<AllLogsResponse?> in
            guard let data = response.body.data else {
                return req.future(nil)
            }

            let decoder = JSONDecoder()
            let response = try decoder.decode(AllLogsResponse.self, from: data)

            return req.future(response)
        }

        request.whenFailure { (error) in
            futResp.fail(error: error)
        }
        
        request.whenSuccess { response in
            guard let logs = response?.rows else {
                futResp.fail(error: NSError(
                    domain: "logs not fetched",
                    code: 1,
                    userInfo: nil
                ))
                return
            }
            
            
            let sorted = logs.sorted { (one, two) -> Bool in
                guard
                    let oneD = one.doc?.datetime,
                    let twoD = two.doc?.datetime
                else {
                    return false
                }
                
                return oneD > twoD
            }
            print(sorted)
                        
            do {
                let renderer = try req.view().render(
                    "logs",
                    [
                        "logs": sorted
                    ]
                )
                
                renderer.whenSuccess { (view) in
                    futResp.succeed(result: view)
                }
                
                renderer.whenFailure { (error) in
                    futResp.fail(error: error)
                }
            } catch let error {
                futResp.fail(error: error)
            }
        }

        return futResp.futureResult
    }
}
