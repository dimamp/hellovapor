//
//  TotalItem.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

struct TotalItem: Codable {
    let id: String
    let is_credit: Bool
    let date: String
    let number: String
    let sum_val: String
    let plat_name: String
    let plat_acc: String
    let pol_name: String
    let pol_acc: String
    let text: String

    let value: Double?
    let dateAsDate: Date?
    var displayValue: String

    var sortBy: (Date, Int) {
        return (dateAsDate!, is_credit ? 0 : 1)
    }

    var cssClass = ""

    init(_ item: Item, source: Item? = nil) {
        value = item.value
        id = "\(item.type)-\(item.id)"
        is_credit = item.isCredit
        date = item.Date
        number = item.number
        text = item.text
        sum_val = item.sum_val

        plat_acc = item.plat_acc
        plat_name = TotalItem.getName(byAccount: plat_acc)

        pol_acc = item.pol_acc
        pol_name = TotalItem.getName(byAccount: pol_acc)

        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd.MM.yyyy"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC")

        dateAsDate = dateFormat.date(from: date)

        if pol_acc == "40817810508390016701" {
            cssClass = "selected"
        }

        displayValue = sum_val

        updateDisplayValue(item, source: source)
    }

    init(_ input: SimpleItem, out: SimpleItem?) {
        value = input.value
        id = "\(input.type)-\(input.id)"
        is_credit = input.isCredit
        date = input.date
        number = input.ref
        sum_val = input.out

        plat_acc = input.acc_num
        plat_name = TotalItem.getName(byAccount: plat_acc)
        pol_acc = out?.acc_num ?? "???"
        pol_name = TotalItem.getName(byAccount: pol_acc)
        text = input.text

        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd.MM.yy"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC")
        
        dateAsDate = dateFormat.date(from: date)

        if pol_acc == "40817810508390016701" {
            cssClass = "selected"
        }

        if let out = out {
            displayValue = (input.currency == out.currency && input.out == out.in)
                ? "\(input.currency)\(input.out)"
                : "\(input.currency)\(input.out) = \(out.currency)\(out.in)"
        } else {
            displayValue = "\(input.currency)\(input.out)"
        }
    }

    mutating func updateDisplayValue(_ item: Item, source: Item? = nil) {
        if let out = source {
            if item.currency == out.currency {
                displayValue = "\(item.currency)\(item.sum_val)"
            } else if let rate = out.getExchangeRate() {
                let resultValue: String

                switch item.currency {
                case "₽": // конвертация долларов в рубли
                    if let value = item.value {
                        resultValue = String(value / rate)
                    } else {
                        resultValue = out.sum_val
                    }

                case "$":
                    if let value = item.value {
                        resultValue = String(value * rate)
                    } else {
                        resultValue = out.sum_val
                    }

                default:
                    resultValue = out.sum_val
                }

                displayValue = "\(item.currency)\(item.sum_val) = \(out.currency)\(resultValue)"
            } else {
                displayValue = "\(item.currency)\(item.sum_val) = \(out.currency)\(out.sum_val)"
            }
        } else {
            displayValue = "\(item.currency)\(item.sum_val)"
        }
    }

    static func getName(byAccount: String) -> String {
        let name: String

        switch byAccount {
        case "40817810508390016701": name = "рублевая карта Димы"
        case "40817810209620014264": name = "рублевая карта Олеси"
        case "40802810023070000221": name = "рублевый счет ИП"
        case "40802840723070000012": name = "валютный счет ИП"
        case "40802840023070000013": name = "транзитный счет ИП"
        case "40817840408390001145": name = "долларовый счет Олеси"
        case "47405840823070000053": name = "рублевый счет ИП"
        default: name = "???"
        }

        return name
    }
}
