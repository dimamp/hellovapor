//
//  Month.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

class Month: Comparable, Codable {
    
    static func < (lhs: Month, rhs: Month) -> Bool {
        return (lhs.year, lhs.month) < (rhs.year, rhs.month)
    }
    
    static func == (lhs: Month, rhs: Month) -> Bool {
        return (lhs.year, lhs.month) == (rhs.year, rhs.month)
    }
    
    let month: Int
    let year: Int

    var tns: MonthTNS? = nil
    var usd: MonthUSD? = nil
    var rur: MonthRUR? = nil
    var usdCard: MonthUSDCard? = nil
    var total: MonthTotal? = nil
    var dima: MonthDima? = nil
    var olesya: MonthOlesya? = nil

    var byType: [AccountType: AccountMonth?] {
        get {
            return [
                .tns: tns,
                .usd: usd,
                .rur: rur,
                .usdCard: usdCard,
                .dima: dima,
                .olesya: olesya
            ]
        }
        set {
            for (key, value) in newValue {
                switch key {
                    case .rur: rur = value as? MonthRUR
                    case .tns: tns = value as? MonthTNS
                    case .usd: usd = value as? MonthUSD
                    case .usdCard: usdCard = value as? MonthUSDCard
                    case .dima: dima = value as? MonthDima
                    case .olesya: olesya = value as? MonthOlesya
                }
            }
        }
    }

    init(month: Int, year: Int) {
        self.month = month
        self.year = year
    }

    func createIfEmpty(type: AccountType, items: [ItemProtocol]) {
        switch type {
            case .rur:
                if rur == nil {
                    rur = MonthRUR(items: items as! [Item])
                }

            case .tns:
                if tns == nil {
                    tns = MonthTNS(items: items as! [Item])
                }
            
            case .usd:
                if usd == nil {
                    usd = MonthUSD(items: items as! [Item])
                }

            case .usdCard:
                if usdCard == nil {
                    usdCard = MonthUSDCard(items: items as! [SimpleItem])
                }

            case .dima:
                if dima == nil {
                    dima = MonthDima(items: items as! [SimpleItem])
                }

            case .olesya:
                if olesya == nil {
                    olesya = MonthOlesya(items: items as! [SimpleItem])
                }

        }
    }

    func createTotal() {
        total = MonthTotal(this: self)
    }
}

