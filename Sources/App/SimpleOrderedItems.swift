//
//  SimpleOrderedItems.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

let SimpleOrderedItems: [String] = [
    "type",
    "acc_num",
    "currency",
    "date",
    "ref",
    "text",
    "in",
    "out",
    "balance"
]
