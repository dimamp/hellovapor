//
//  MyCouchDB.swift
//  App
//
//  Created by Dmitriy Baranov on 12/16/19.
//

import Foundation
import Vapor

struct LogResponse: Codable {
    var id: String
    var key: String
    var value: DocValue
    var doc: LogItem?
}

struct AllLogsResponse: Codable {
    var total_rows: Int
    var offset: Int
    var rows: [LogResponse]
}

struct LogItemChange: Codable {
    var field: String
    var old: String
    var new: String
}

struct LogItem: Codable {
    var datetime: String
    var changes: [LogItemChange]
    var user: String
}

struct MyCouchDBFindRequest<T: Codable>: Codable {
    var selector: [String: T]
    var fields: [String]?
    var sort: [[String: String]]?
    var limit: Int?
    var skip: Int?
}

struct MyCouchDBFindResponse: Codable {
    var docs: [Item]
    var warning: String?
    var bookmark: String?
}

struct OnUpdateResponse: Codable {
    var ok: Bool
    var id: String
    var rev: String
}

struct DocValue: Codable {
    var rev: String
}

struct DocResponse: Codable {
    var id: String
    var key: String
    var value: DocValue
    var doc: Item?
}

struct AllDocsResponse: Codable {
    var total_rows: Int
    var offset: Int
    var rows: [DocResponse]
}

class MyCouchDB {
    struct ConnectionProperties {
        var secure: Bool
        var hostname: String
        var username: String?
        var password: String?
        var url: String {
            return (secure
                ? "https://"
                : "http://"
            ) + hostname + "/"
        }

        var authHeader: String? {
            guard
                let username = username,
                let password = password
            else {
                return nil
            }

            let token = Data("\(username):\(password)".utf8)

            return token.base64EncodedString(options: [])
        }
    }

    private let worker: Worker
    private let properties: ConnectionProperties

    init(connectionProperties: ConnectionProperties, worker: Worker) {
        self.worker = worker
        self.properties = connectionProperties
    }

    func connect(timeout: Int) -> EventLoopFuture<HTTPClient> {
        let scheme: HTTPScheme = properties.secure
            ? .https
            : .http

        return HTTPClient.connect(
            scheme: scheme,
            hostname: properties.hostname,
            port: properties.secure ? 443 : 80,
            connectTimeout: TimeAmount.seconds(timeout),
            on: worker,
            onError: { error in
                let f: () -> EventLoopFuture<HTTPClient> = {
                    return self.worker.future(error: error)
                }
                _ = f()
            }
        )
    }

    func getAllDb() -> HTTPRequest {
        var request = HTTPRequest(
            method: .GET,
            url: properties.url + "_all_dbs"
        )

        if let authHeader = properties.authHeader {
            request.headers.add(
                name: HTTPHeaderName.authorization,
                value: "Basic \(authHeader)"
            )
        }

        return request
    }

    func getAllDocs(database: String) -> HTTPRequest {
        var request = HTTPRequest(
            method: .GET,
            url: properties.url + "\(database)/_all_docs?include_docs=true"
        )

        if let authHeader = properties.authHeader {
            request.headers.add(
                name: HTTPHeaderName.authorization,
                value: "Basic \(authHeader)"
            )
        }

        return request
    }

    func getDoc(database: String, docId: String) -> HTTPRequest {
        var request = HTTPRequest(
            method: .GET,
            url: properties.url + "\(database)/\(docId)"
        )

        if let authHeader = properties.authHeader {
            request.headers.add(
                name: HTTPHeaderName.authorization,
                value: "Basic \(authHeader)"
            )
        }

        return request
    }

    func update(
        database: String,
        doc: Item
    ) throws -> HTTPRequest {
        var request = HTTPRequest(
            method: .PUT,
            url: properties.url + "\(database)/\(doc._id!)/",
            body: try JSONEncoder().encode(doc)
        )

        if let authHeader = properties.authHeader {
            request.headers.add(
                name: HTTPHeaderName.authorization,
                value: "Basic \(authHeader)"
            )
        }

        return request
    }
    
    func find<T: Codable>(
        database: String,
        selector: [String: T]
    ) throws -> HTTPRequest {
        let find = MyCouchDBFindRequest(selector: selector)
        let body = try JSONEncoder().encode(find)        
        
        var request = HTTPRequest(
            method: .POST,
            url: properties.url + "\(database)/_find",
            body: body
        )
        
        request.headers.add(
            name: HTTPHeaderName.contentType,
            value: "application/json"
        )

        if let authHeader = properties.authHeader {
            request.headers.add(
                name: HTTPHeaderName.authorization,
                value: "Basic \(authHeader)"
            )
        }

        return request
    }
    
    func log(
        dbName: String,
        doc: LogItem
    ) throws -> HTTPRequest {
        var request = HTTPRequest(
            method: .POST,
            url: properties.url + dbName,
            body: try JSONEncoder().encode(doc)
        )
        
        request.headers.add(
            name: HTTPHeaderName.contentType,
            value: "application/json"
        )

        if let authHeader = properties.authHeader {
            request.headers.add(
                name: HTTPHeaderName.authorization,
                value: "Basic \(authHeader)"
            )
        }

        return request
    }
}
