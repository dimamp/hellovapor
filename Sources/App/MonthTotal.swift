//
//  MonthTotal.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

class MonthTotal: Codable {
    weak var thisMonth: Month!

    var items: [TotalItem] = []

    init(this: Month) {
        thisMonth = this
        calc()
    }

    func calc() {
        calcTNS()
        calcUSD()
        calcRUR()
        calcUSDCard()
        calcOlesya()
        calcDima()

        items.sort { (lhs, rhs) -> Bool in
            lhs.sortBy < rhs.sortBy
        }
    }

    func calcTNS() {
        thisMonth.tns?.items.forEach { tns in
            self.items.append(TotalItem(tns))
        }
    }

    func calcUSD() {
        thisMonth.usd?.items.forEach { usd in
            // продажа валюты
            let toUSDsell = !usd.isCredit && usd.pol_acc == "47408840823070000005"

            if toUSDsell {
                self.items.append(TotalItem(usd))
            }
        }
    }

    func calcRUR() {
        thisMonth.rur?.items.forEach { rur in
            // Рублевая карта Димы
            let toRURdima = !rur.isCredit && rur.pol_acc == "40817810508390016701"

            if toRURdima {
                var source: Item? = nil

                self.thisMonth.rur?.usd_to_rur_to_dima.forEach { dep in
                    if dep.rur_to_user == rur.id {
                        if let usd_to_rur = self.thisMonth.rur?.items.first(where: {
                            $0.id == dep.usd_to_rur
                        }) {
                            self.items.append(TotalItem(usd_to_rur))

                            source = self.thisMonth.usd?.items.first(where: {
                                $0.number == usd_to_rur.number
                            })
                        }

                        if let conversion_fee = self.thisMonth.rur?.items.first(where: {
                            $0.id == dep.conversion_fee
                        }) {
                            self.items.append(TotalItem(conversion_fee))
                        }
                    }
                }

                self.items.append(TotalItem(rur, source: source))
            }
        }
    }

    func calcUSDCard() {
        thisMonth.usdCard?.items.forEach { card in
            // перевод на рублевый счет
            if let dima: SimpleItem = thisMonth.dima?.items.first(where: {
                (dima) -> Bool in dima.ref == card.ref
            }), !card.isCredit {
                self.items.append(TotalItem(card, out: dima))
            }

            // снятие наличных
            // let toDimaCash = false
        }
    }

    func calcOlesya() {
        thisMonth.olesya?.items.forEach { olesya in
            // перевод на рублевый счет Димы
            if let dima: SimpleItem = thisMonth.dima?.items.first(where: {
                (dima) -> Bool in dima.ref == olesya.ref
                    && dima.dateAsDate == olesya.dateAsDate
            }), !olesya.isCredit {
                self.items.append(TotalItem(olesya, out: dima))
            }
        }
    }

    func calcDima() {
        thisMonth.dima?.items.forEach { dima in
            // перевод с рублевого счета
            if !dima.isCredit {
                if items.contains(where: { (it) -> Bool in
                    return it.value == dima.value
                        && it.dateAsDate == dima.dateAsDate
                }) {
                    let olesya = thisMonth.olesya?.items.first(where: {
                        (olesya) -> Bool in dima.ref == olesya.ref
                            && dima.value == olesya.value
                            && dima.dateAsDate == olesya.dateAsDate

                    })

                    self.items.append(TotalItem(dima, out: olesya))
                }
            }
        }
    }
}
