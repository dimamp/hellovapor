//
//  AccountMonth.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

protocol AccountMonth: Codable {
    var rows: [ItemProtocol] {get}

    func set(balance: Double, forRow: Int)
}
