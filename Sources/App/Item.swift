//
//  Item.swift
//  App
//
//  Created by Dima on 19.12.2019.
//

import Foundation
import Vapor

enum Person: String, Codable {
    case dima, olesya, unknown, both
}

enum FeeType: String, Codable {
    case unknown, serviceFee, transfer, transferFee, conversion, conversionFee, english, elba, taxes, olesya_credit, olesya_debit
}

struct Item: ItemProtocol, Content {
    
    enum CodingKeys: String, CodingKey {
        case Date,
            `Type`,
            bc_val,
            d_c,
            number,
            sum_val,
            plat_name,
            plat_acc,
            pol_name,
            pol_acc,
            text70,
            
            isCredit,
            currency,
            id,
            cssClass,
            type,
            cssStyle,
        
            _id,
            _rev,
            moneyOwner,
            feeType,
            spent,
            conversionChargeNumber,
            transferId,
            conversionId,
                    
            valueAsString,
            formatBalance,
            plat_descr,
            pol_descr
    }
    
    let accounts: [String: String] = [
        "40802810023070000221": "рублевый счет ИП",
        "40802840723070000012": "валютный счет ИП",
        "40802840023070000013": "транзитный счет ИП",
        "40817840408390001145": "долларовая карта Олеси",
        "40817810508390016701": "Рублевая карта Димы",
        "40817810209620014264": "Рублевая карта Олеси",
        "47405810623070000050": "транзитный счет ИП (продажа)"
    ]
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        Date = try values.decode(String.self, forKey: .Date)
        `Type` = try values.decode(String.self, forKey: .`Type`)
        bc_val = try values.decode(String.self, forKey: .bc_val)
        d_c = try values.decode(String.self, forKey: .d_c)
        number = try values.decode(String.self, forKey: .number)
        sum_val = try values.decode(String.self, forKey: .sum_val)
        plat_name = try values.decode(String.self, forKey: .plat_name)
        plat_acc = try values.decode(String.self, forKey: .plat_acc)
        pol_name = try values.decode(String.self, forKey: .pol_name)
        pol_acc = try values.decode(String.self, forKey: .pol_acc)
        text70 = try values.decode(String.self, forKey: .text70)
        
        isCredit = try values.decode(Bool.self, forKey: .isCredit)
        currency = try values.decode(String.self, forKey: .currency)
        id = try values.decode(Int.self, forKey: .id)
        cssClass = try values.decode(String.self, forKey: .cssClass)
        type = try values.decode(AccountType.self, forKey: .type)
        cssStyle = try values.decode(String.self, forKey: .cssStyle)
        
        _id = try values.decode(String.self, forKey: ._id)
        _rev = try values.decode(String.self, forKey: ._rev)
        moneyOwner = try values.decode(Person.self, forKey: .moneyOwner)
        feeType = try values.decode(FeeType.self, forKey: .feeType)
        spent = try values.decode(Double.self, forKey: .spent)
        conversionChargeNumber = try? values.decode(
            String.self,
            forKey: .conversionChargeNumber
        )
        transferId = try? values.decode(Int.self, forKey: .transferId)
        conversionId = try? values.decode(Int.self, forKey: .conversionId)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(Date, forKey: .Date)
        try container.encode(`Type`, forKey: .`Type`)
        try container.encode(bc_val, forKey: .bc_val)
        try container.encode(d_c, forKey: .d_c)
        try container.encode(number, forKey: .number)
        try container.encode(sum_val, forKey: .sum_val)
        try container.encode(plat_name, forKey: .plat_name)
        try container.encode(plat_acc, forKey: .plat_acc)
        try container.encode(pol_name, forKey: .pol_name)
        try container.encode(pol_acc, forKey: .pol_acc)
        try container.encode(text70, forKey: .text70)
        
        try container.encode(isCredit, forKey: .isCredit)
        try container.encode(currency, forKey: .currency)
        try container.encode(id, forKey: .id)
        try container.encode(cssClass, forKey: .cssClass)
        try container.encode(type, forKey: .type)
        try container.encode(cssStyle, forKey: .cssStyle)
        
        try container.encode(_id, forKey: ._id)
        try container.encode(_rev, forKey: ._rev)
        try container.encode(moneyOwner, forKey: .moneyOwner)
        try container.encode(feeType, forKey: .feeType)
        try container.encode(spent, forKey: .spent)
        
        try container.encode(valueAsString, forKey: .valueAsString)
        try container.encode(formatBalance, forKey: .formatBalance)
        try container.encode(plat_descr, forKey: .plat_descr)
        try container.encode(pol_descr, forKey: .pol_descr)
        
        try container.encode(conversionChargeNumber, forKey: .conversionChargeNumber)
        try container.encode(transferId, forKey: .transferId)
        try container.encode(conversionId, forKey: .conversionId)
    }
    
    var _id: String? = nil
    var _rev: String? = nil

    var moneyOwner: Person = .unknown
    var feeType: FeeType = .unknown
    var conversionChargeNumber: String? = nil
    var transferId: Int? = nil
    var conversionId: Int? = nil
    var spent: Double = 0

    // Дата выписки
    let Date: String

    // Код валюты счета
    let `Type`: String

    // Входящий ост. по кред. в вал. счета
    let bc_val: String

    // Признак дебет/кредит
    let d_c: String

    // Номер док-та
    let number: String

    // Сумма док-та, в вал. счета
    let sum_val: String

    // Плательщик
    let plat_name: String

    // Расчетный счет плательщика
    let plat_acc: String

    // Получатель
    let pol_name: String

    // Расчетный счет получателя
    let pol_acc: String

    // Назначение платежа
    var text70: String

    let isCredit: Bool
    let currency: String
    
    ///
    var balance: Double? = nil
    var value: Double? {
        return Double(sum_val.replacingOccurrences(of: ",", with: "."))
    }
    var initialValue: Double? {
        return Double(bc_val.replacingOccurrences(of: ",", with: "."))
    }
    var dateAsDate: Date? {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd.MM.yyyy"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC")

        return dateFormat.date(from: Date)
    }
    var month: Int? {
        if let date = dateAsDate {
            return Calendar.current.component(.month, from: date)
        } else {
            return nil
        }
    }
    var year: Int? {
        if let date = dateAsDate {
            return Calendar.current.component(.year, from: date)
        } else {
            return nil
        }
    }
    var day: Int? {
        if let date = dateAsDate {
            return Calendar.current.component(.day, from: date)
        } else {
            return nil
        }
    }
    var sortBy: (Date, String) {
        return (dateAsDate!, d_c)
    }
    var text: String {
        get { return text70 }
        set { text70 = newValue }
    }
    var valueAsString: String {
        return sum_val
    }
    var formatBalance: String {
        let item = self
        guard
            let balance = item.balance,
            let sum = item.value
            else { return "[ERROR]" }

        let total = item.isCredit
            ? balance - sum
            : balance + sum

        let sign = item.isCredit ? "+" : "-"
        var conv: String = ""
        
        if let rate = item.getExchangeRate() {
            if item.currency == "$" {
                conv = "&nbsp;<small><b>($"
                    + String(format: "%.2f", sum * rate)
                    + ")</b></small>&nbsp;"
            } else {
                conv = "&nbsp;<small><b>($"
                    + String(format: "%.2f", sum / rate)
                    + ")</b></small>&nbsp;"
            }
        }

        return String(format: "%.2f", total)
            + "<br/>"
            + sign + "&nbsp;" + item.valueAsString + conv
            + "<br/>"
            + "=&nbsp;"
            + String(format: "%.2f", balance)
    }
    
    var plat_descr: String {
        return accounts[plat_acc] ?? ""
    }
    
    var pol_descr: String {
        return accounts[pol_acc] ?? ""
    }

    let id: Int
    var cssClass = ""
    let type: AccountType
    var cssStyle = ""
   
    init(id: Int, type: AccountType, line: [Substring]) {
        self.id = id
        self.type = type

        let get: (String) -> String = {
            if let index = OrderedItems.index(of: $0), index < line.count {
                return String(line[index])
            } else {
                return ""
            }
        }
        
        Date = get("Date")
        Type = get("Type")
        bc_val = get("bc_val")
        d_c = get("d_c")
        number = get("number")
        sum_val = get("sum_val")
        plat_name = get("plat_name")
        plat_acc = get("plat_acc")
        pol_name = get("pol_name")
        pol_acc = get("pol_acc")
        text70 = get("text70")
        
        isCredit = d_c == "C"
        switch type {
            case .dima, .olesya, .rur: currency = "₽"
            case .tns, .usd, .usdCard: currency = "$"
        }
    }

    mutating func setFeeType() {
        let item = self

        if item.text.contains("Комиссия за обслуживание счета") {
            feeType = .serviceFee
            moneyOwner = .both
        } else if item.text.contains("Комиссия за сервис \"Мобильный платеж\"")  {
            feeType = .serviceFee
            moneyOwner = .both
        } else if item.text.contains("Погашение задолженности по комиссии за обслуживание счета") {
            feeType = .serviceFee
            moneyOwner = .both
        } else if item.text.contains("Комиссия за обслуживание р/с") {
            feeType = .serviceFee
            moneyOwner = .both
        } else if item.text.contains("Комиссия за конвертацию") {
            feeType = .conversionFee
            setConversionFeeChargeNumber()
        } else if item.text.contains("Комиссия за внутренние переводы") {
            feeType = .transferFee
        } else if item.text.contains("Ком.за вып.функций агента") {
            feeType = .serviceFee
            moneyOwner = .both
            setConversionFeeChargeNumber()
        } else if item.text.contains("Комиссия за переводы в рублях") {
            feeType = .transferFee
        } else if item.text.contains("Пополнение счета") {
            feeType = .transfer
        } else if item.text.contains("Конвертация") {
            feeType = .conversion
            setConversionChargeNumber()
            setConversionMoneyOwner()
        } else if item.text.contains("Зачисление средств после продажи USD") {
            feeType = .conversion
            setConversionChargeNumber()
            setConversionMoneyOwner()
        } else if item.text.contains("Списание комиссии FX РКО") {
            feeType = .conversionFee
        } else if item.pol_name.contains("ЛИНГВА ПРО") {
            feeType = .english
            moneyOwner = .olesya
        } else if item.pol_name.contains("СКБ КОНТУР") {
            feeType = .elba
            moneyOwner = .olesya
        } else {
            feeType = .unknown
        }
    }

    mutating func setConversionMoneyOwner() {
        let item = self

        if item.isCredit, item.feeType == .conversion {
            guard
                let rurValue = item.value,
                let usdRate = getExchangeRate()
            else {
                moneyOwner = .unknown
                print("ВНИМАНИЕ: у конвертации за \(item.Date) не удалось получить курс")
                return
            }

            let usdValue = rurValue / usdRate

            let known = knownConversions.filter { (conv) -> Bool in
                if conv.date == item.dateAsDate {
                    return usdValue >= conv.sum - 10
                }
                return false
            }

            if known.count > 0 {
                if known.count > 1 {
                    print("ВНИМАНИЕ: больше одной конвертации в день \(item.Date)")
                    moneyOwner = .unknown
                } else {
                    let diff = abs(usdValue - known.first!.sum)

                    if diff > 10 {
                        moneyOwner = .both
                    } else {
                        moneyOwner = known.first!.to
                    }
                }
            } else {
                moneyOwner = .olesya
            }
        } else {
            moneyOwner = .unknown
        }
    }

    mutating func setConversionChargeNumber() {
        let item = self

        if let range = item.text.range(
            of: "Поруч.№ ([0-9]+)",
            options: .regularExpression
        ) {
            var value = String(item.text[range])

            value = value.replacingOccurrences(
                of: "Поруч.№ ",
                with: ""
            )

            conversionChargeNumber = value
            conversionId = id
        } else if let range = item.text.range(
            of: "Распоряжение №([0-9]+)",
            options: .regularExpression
            ) {
            var value = String(item.text[range])

            value = value.replacingOccurrences(
                of: "Распоряжение №",
                with: ""
            )

            conversionChargeNumber = value
            conversionId = id
        } else {
            print("Внимание: Конвертация от \(Date) - Невозможно получить номер поручения!")
        }
    }

    mutating func setConversionFeeChargeNumber() {
        let item = self

        if let range = item.text.range(
            of: "Комиссия за конвертацию по поручению N ([0-9]+)",
            options: .regularExpression
        ) {
            var value = String(item.text[range])

            value = value.replacingOccurrences(
                of: "Комиссия за конвертацию по поручению N ",
                with: ""
            )

            conversionChargeNumber = value
        } else if let range = item.text.range(
            of: "Комиссия за конвертацию по поручению №([0-9]+)",
            options: .regularExpression
        ) {
            var value = String(item.text[range])

            value = value.replacingOccurrences(
                of: "Комиссия за конвертацию по поручению №",
                with: ""
            )

            conversionChargeNumber = value
        } else if let range = item.text.range(
            of: "расп. № ([0-9]+)",
            options: .regularExpression
            ) {
            var value = String(item.text[range])

            value = value.replacingOccurrences(
                of: "расп. № ",
                with: ""
            )

            conversionChargeNumber = value
        } else {
            print("Внимание: Комиссия за конвертацию от \(Date) - Невозможно получить номер поручения!")
        }
    }

    mutating func setTransferFeeId(items: [Item]) {
        let item = self

        if item.feeType != .transferFee {
            return
        }

        print(item.text)

        var date: Date? = nil

        if item.text.contains("Комиссия за внутренние переводы в валюте РФ на счета ФЛ") {
            if let range = item.text.range(
                of: "\\d{2}\\w{3}\\d{2}",
                options: .regularExpression
            ) {
                date = parseDate(from: String(item.text[range]))
            }
        } else if item.text.contains("Комиссия за переводы в рублях за период") {
            if let range1 = item.text.range(
                of: "\\d{2}\\w{3}\\d{2}",
                options: .regularExpression
            ) {
                let date1 = parseDate(from: String(item.text[range1]))

                if let range2 = item.text.range(
                    of: "\\d{2}\\w{3}\\d{2}",
                    options: .regularExpression,
                    range: range1.upperBound..<item.text.endIndex
                ) {
                    let date2 = parseDate(from: String(item.text[range2]))

                    if date1 == date2 {
                        print("bingo!!!!")

                        date = date1
                    }
                }
            }
        }

        print(date)

        if let date = date {
            let transfers = items.filter { (i) -> Bool in
                i.dateAsDate == date && i.feeType == .transfer
            }

            if transfers.count == 1 {
                transferId = transfers.first!.id
                moneyOwner = transfers.first!.moneyOwner
            }
        } else {
            print("Внимание: Комиссия за перевод от \(Date) - Невозможно получить ID перевода!")
        }
    }

    private func parseDate(from text: String) -> Date? {
        let monthAbbr = [
            "ЯНВ": 1,
            "ФЕВ": 2,
            "МАР": 3,
            "АПР": 4,
            "МАЙ": 5,
            "ИЮН": 6,
            "ИЮЛ": 7,
            "АВГ": 8,
            "СЕН": 9,
            "ОКТ": 10,
            "НОЯ": 11,
            "ДЕК": 12
        ]
        let monthStart = text.index(text.startIndex, offsetBy: 2)
        let monthEnd = text.index(text.endIndex, offsetBy: -2)
        let range = monthStart..<monthEnd
        let monthName = String(text[range])
        let month = monthAbbr[monthName]!
        let day = text.substring(to: monthStart)
        let year = text.substring(from: monthEnd)

        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd.M.yy"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC")

        let date = dateFormat.date(from: "\(day).\(month).\(year)")

        return date
    }

    mutating func setConversionFeeOwner(items: [Item]) {
        let item = self

        if item.feeType == .conversionFee {
            if let n = item.conversionChargeNumber {
                let convertions = items.filter { (i) -> Bool in
                    if i.feeType == .conversion {
                        return i.conversionChargeNumber == n
                            && i.year == item.year
                            && i.month == item.month
                    } else {
                        return false
                    }
                }

                if convertions.count == 0 {
                    print("Внимание: Не найдено ни одной конвертации для комиссии id: \(item.id)")
                } else if convertions.count == 1 {
                    let cnv = convertions.first!
                    self.moneyOwner = cnv.moneyOwner
                    self.conversionId = cnv.id
                } else {
                    print("Внимание: Найдено больше одной конвертации для комиссии id: \(item.id)")
                    print(convertions.map({ (c) -> Int in c.id }))
                }
            }
        }
    }

    /// return conversion id
    mutating func setupTransferConnection(items: [Item]) -> Int? {
        let item = self

        if item.feeType == .transfer {
            let convertions = items.filter { (i) -> Bool in
                if i.feeType == .conversion
                    && i.year == item.year
                    && i.month == item.month
                    && i.day! <= item.day!
                    && i.transferId == nil
                {
                    let diff = abs(i.value! - item.value!)
                    if diff < (i.value! / 100) * 10 {
                        // no more than 10% diff between convertion and transfer sums
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            }

            if convertions.count == 0 {
                print("Внимание: Не найдено ни одной конвертации для Перевод id: \(item.id)")
            } else if convertions.count == 1 {
                let cnv = convertions.first!
                self.moneyOwner = cnv.moneyOwner
                self.conversionChargeNumber = cnv.conversionChargeNumber
                self.conversionId = cnv.id
                return cnv.id
            } else {
                print("Внимание: Найдено больше одной конвертации для Перевод id: \(item.id)")
                print(convertions.map({ (c) -> Int in c.id }))
            }
        }

        return nil
    }
    
    func getExchangeRate() -> Double? {
        let item = self
        
        if item.feeType == .olesya_credit {
            return 1
        }

        if let range = item.text.range(
            of: "Курс\\s?-\\s?([0-9.]+)\\,?",
            options: .regularExpression
        ) {
            var value = String(item.text[range])

            value = value.replacingOccurrences(
                of: "Курс - ",
                with: ""
            )

            value = value.replacingOccurrences(
                of: "Курс-",
                with: ""
            )

            value = value.replacingOccurrences(
                of: ",",
                with: ""
            )

            return Double(value)
        }

        return nil
    }
}
