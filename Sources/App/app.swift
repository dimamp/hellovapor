import Vapor

let colorsList: [String] = [
    "red", "green", "yellow", "royalblue", "orange", "cyan", "magenta",
    "lime", "teal", "lavender", "brown", "beige", "olive"
]
var colorsListCounter = 0

func randomColor() -> String {
    colorsListCounter = colorsListCounter >= colorsList.count - 1
        ? 0
        : colorsListCounter + 1

    return colorsList[colorsListCounter]
}

/// Creates an instance of `Application`. This is called from `main.swift` in the run target.
public func app(_ env: Environment) throws -> Application {
    var config = Config.default()
    var env = env
    var services = Services.default()
    try configure(&config, &env, &services)
    let app = try Application(config: config, environment: env, services: services)
    try boot(app)
    return app
}
