//
//  MonthTNS.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

class MonthTNS: AccountMonth {

    enum OutAccNum: String {
        case usd = "40802840723070000012"
        case rur = "47405840823070000053"
    }

    var rows: [ItemProtocol] {
        return items
    }
    var items: [Item] = []
    var input = Value()
    
    var dima_input: Double = 0
    var olesya_input: Double = 0
    var other_input: Double = 0
    
    var out_to_usd = Value()
    var out_to_rur = Value()

    var balance: Double = 0
    
    init(items: [Item]) {
        self.items = items
    }
    
    func calc(prevMonth: Month?, inputBalance: Double) {
        calcInput(prevMonth)
        calcOutput(prevMonth)
        // баланс
        let prev_balance = prevMonth?.tns?.balance ?? inputBalance
        balance = prev_balance + input.now
            - out_to_rur.now
            - out_to_usd.now
    }

    func goesOut(item: Item, to: OutAccNum) -> Bool {
        return (item.d_c == "D" && item.pol_acc == to.rawValue)
    }

    private func calcOutput(_ prevMonth: Month?) {
        // перевод на валютный счет ИП
        out_to_usd.now = items.reduce(0) { (result, item) -> Double in
            goesOut(item: item, to: .usd) ? result + item.value! : result
        }
        out_to_usd.prev = prevMonth?.tns?.out_to_usd.total ?? 0
        out_to_usd.total = out_to_usd.prev + out_to_usd.now
        
        // конвертация на рублевый счет ИП
        out_to_rur.now = items.reduce(0) { (result, item) -> Double in
            goesOut(item: item, to: .rur) ? result + item.value! : result
        }
        out_to_rur.prev = prevMonth?.tns?.out_to_rur.total ?? 0
        out_to_rur.total = out_to_rur.prev + out_to_rur.now
    }
    
    private func calcInput(_ prevMonth: Month?) {
        // суммарный приход
        input.now = items.reduce(0) { (result, item) -> Double in
            return (item.d_c == "C")
                ? result + item.value!
                : result
        }
        input.prev = prevMonth?.tns?.input.total ?? 0
        input.total = input.prev + input.now
        
        // ЗП Дима
        let dima_prev_input = prevMonth?.tns?.dima_input ?? 0
        dima_input = dima_prev_input + items.reduce(0) { (result, item) -> Double in
            let fromCA = item.plat_name.contains("CLUB AUTOMATION")
            let fromDaxko = item.plat_name.contains("DAXKO")
            let value = item.value!
            if item.d_c == "C" && (fromCA || fromDaxko) {
                if value >= 5200 {
                    return result + 2700
                } else if value >= 5175 && value < 5200 {
                    return 2700 - ((5200 - value) / 2)
                } else {
                    return result
                }
            } else {
                return result
            }
        }

        // ЗП Олеся
        let olesya_prev_input = prevMonth?.tns?.olesya_input ?? 0
        olesya_input = olesya_prev_input + items.reduce(0) { (result, item) -> Double in
            let fromCA = item.plat_name.contains("CLUB AUTOMATION")
            let fromDaxko = item.plat_name.contains("DAXKO")
            let value = item.value!
            if item.d_c == "C" && (fromCA || fromDaxko) {
                if value >= 5200 {
                    return result + 2500
                } else if value >= 5175 && value < 5200 {
                    return 2500 - ((5200 - value) / 2)
                } else {
                    return result
                }
            } else {
                return result
            }
        }
        
        // приход - ЗП
        other_input = input.prev + input.now
            - dima_input
            - olesya_input
    }

    func set(balance: Double, forRow: Int) {
        items[forRow].balance = balance
    }
}
