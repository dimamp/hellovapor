//
//  MonthRURInput.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

struct MonthRURInput: Codable {
    // поступления с валютного счета (сумма в рублях)
    var usd_rur = Value()
    // поступления с валютного счета (сумма в долларах)
    var usd_usd = Value()
    // прочие поступления
    var other = Value()

    static let usdAccounts = [
        "40802840723070000012",
        "47405810623070000050",
    ]

    var dima_usd = Value()
    var dima_rur = Value()
    var olesya_usd = Value()
    var olesya_rur = Value()
    var olesya_other = Value()

    mutating func calc(
        _ month: MonthRUR,
        inputBalance: Double,
        prevMonth: Month?,
        nextMonth: Month?
    ) {
        //
        dima_rur.now = month.items.reduce(0) { (result, item) -> Double in
            if item.feeType == .conversion && item.moneyOwner == .dima {
                return result + item.value!
            } else {
                return result
            }
        }

        dima_rur.prev = prevMonth?.rur?.input.dima_rur.total ?? inputBalance
        dima_rur.total = dima_rur.now + dima_rur.prev

        //
        dima_usd.now = month.items.reduce(0) { (result, item) -> Double in
            if item.feeType == .conversion && item.moneyOwner == .dima {
                return result + item.value! / item.getExchangeRate()!
            } else {
                return result
            }
        }
        dima_usd.now = Double(String(format: "%.2f", dima_usd.now))!
        dima_usd.prev = prevMonth?.rur?.input.dima_usd.total ?? 0
        dima_usd.total = dima_usd.now + dima_usd.prev

        //
        olesya_rur.now = month.items.reduce(0) { (result, item) -> Double in
            if item.feeType == .conversion && item.moneyOwner == .olesya {
                return result + item.value!
            } else {
                return result
            }
        }

        olesya_rur.prev = prevMonth?.rur?.input.olesya_rur.total ?? inputBalance
        olesya_rur.total = olesya_rur.now + olesya_rur.prev

        //
        olesya_usd.now = month.items.reduce(0) { (result, item) -> Double in
            if item.feeType == .conversion && item.moneyOwner == .olesya {
                return result + item.value! / item.getExchangeRate()!
            } else {
                return result
            }
        }
        olesya_usd.now = Double(String(format: "%.2f", olesya_usd.now))!
        olesya_usd.prev = prevMonth?.rur?.input.olesya_usd.total ?? 0
        olesya_usd.total = olesya_usd.now + olesya_usd.prev

        //
        olesya_other.now = month.items.reduce(0) { (result, item) -> Double in
            if item.feeType == .olesya_credit && item.moneyOwner == .olesya {
                return result + item.value!
            } else {
                return result
            }
        }

        olesya_other.prev = prevMonth?.rur?.input.olesya_other.total ?? inputBalance
        olesya_other.total = olesya_other.now + olesya_other.prev

        //
        month.items.forEach { (item) in
            if item.feeType == .conversion && item.moneyOwner == .both {
                var usd = item.value! / item.getExchangeRate()!
                usd = Double(String(format: "%.2f", usd))!

                let kcs = knownConversions.filter({ (kc) -> Bool in
                    kc.date == item.dateAsDate && kc.sum <= usd
                })


                if kcs.count != 1 {
                    print("!!! Общая конвертацию id=\(item.id) на сумму \(item.value)")
                } else {
                    let dimaPart = kcs.first!.to == .dima
                        ? kcs.first!.sum
                        : usd - kcs.first!.sum

                    let olesyaPart = usd - dimaPart

                    dima_usd.now +=  dimaPart
                    dima_usd.total = dima_usd.now + dima_usd.prev

                    dima_rur.now += item.value! / usd * dimaPart
                    dima_rur.total = dima_rur.now + dima_rur.prev

                    olesya_usd.now +=  olesyaPart
                    olesya_usd.total = olesya_usd.now + olesya_usd.prev

                    olesya_rur.now += item.value! / usd * olesyaPart
                    olesya_rur.total = olesya_rur.now + olesya_rur.prev
                }
            }
        }

    }

    mutating func old_calc(
        _ month: MonthRUR,
        inputBalance: Double,
        prevMonth: Month?,
        nextMonth: Month?
    ) {
        // поступления с валютного счета (сумма в рублях)
        usd_rur.now = month.items.reduce(0) { (result, item) -> Double in
            if item.isCredit && MonthRURInput.usdAccounts.contains(item.plat_acc) {
                return result + item.value!
            } else {
                return result
            }
        }

        usd_rur.prev = prevMonth?.rur?.input.usd_rur.total ?? inputBalance
        usd_rur.total = usd_rur.now + usd_rur.prev

        // поступления с валютного счета (сумма в долларах)
        usd_usd.now = month.items.reduce(0) { (result, item) -> Double in
            if item.isCredit && MonthRURInput.usdAccounts.contains(item.plat_acc) {
                return result + item.value! / item.getExchangeRate()!
            } else {
                return result
            }
        }
        usd_usd.now = Double(String(format: "%.2f", usd_usd.now))!
        usd_usd.prev = prevMonth?.rur?.input.usd_usd.total ?? 0
        usd_usd.total = usd_usd.now + usd_usd.prev

        // прочие поступления
        other.now = month.items.reduce(0) { (result, item) -> Double in
            if item.isCredit && !MonthRURInput.usdAccounts.contains(item.plat_acc) {
                return result + item.value!
            } else {
                return result
            }
        }

        other.now = Double(String(format: "%.2f", other.now))!
        other.prev = prevMonth?.rur?.input.other.total ?? inputBalance
        other.total = other.now + other.prev
    }
}

