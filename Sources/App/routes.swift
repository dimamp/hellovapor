import Vapor
import Authentication
import Crypto
import HTTP

final class MyBasicAuth: BasicAuthenticatable {
    var username: String = ""
    var password: String = ""
    static var usernameKey: WritableKeyPath<MyBasicAuth, String> = \.username
    static var passwordKey: WritableKeyPath<MyBasicAuth, String> = \.password

    static func authenticate(
        using basic: BasicAuthorization,
        verifier: PasswordVerifier,
        on connection: DatabaseConnectable
    ) -> EventLoopFuture<MyBasicAuth?> {
        let auth = MyBasicAuth()
        auth.username = basic.username
        auth.password = basic.password
        return connection.future(auth)
    }

    public static func basicAuthMiddleware(
        using verifier: PasswordVerifier
    ) -> BasicAuthenticationMiddleware<MyBasicAuth> {
        return BasicAuthenticationMiddleware(verifier: verifier)
    }
}

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
    router.get { req in
        return "It works!"
    }
    
    // Basic "Hello, world!" example
    router.get("hello") { req -> String in
        let db = MyCouchDB(
            connectionProperties: MyCouchDB.ConnectionProperties(
                secure: true,
                hostname: "c3e79186-40a7-4665-add7-41eb3a5bd08f-bluemix"
                    + ".cloudantnosqldb.appdomain.cloud"
            ),
            worker: req
        )

        let client = db.connect(timeout: 30)
        client.catch { (error) in
            print("EEEERRRRORORO!")
            print(error)
        }

        let request = client.flatMap { (client) -> Future<HTTPResponse> in
            client.send(db.getAllDb())
        }.flatMap { (response) -> EventLoopFuture<[String]?> in
            guard let data = response.body.data else {
                return req.future(nil)
            }

            let decoder = JSONDecoder()
            let response = try decoder.decode([String].self, from: data)

            return req.future(response)
        }

        request.whenFailure { (error) in
            print(error)
        }

        request.whenSuccess { (response) in
            print(response)
        }

        //

        let request2 = client.flatMap { (client) -> Future<HTTPResponse> in
            client.send(db.getAllDocs(database: "rur_2019"))
        }.flatMap { (response) -> EventLoopFuture<AllDocsResponse?> in
            guard let data = response.body.data else {
                return req.future(nil)
            }

            let decoder = JSONDecoder()
            let response = try decoder.decode(AllDocsResponse.self, from: data)

            return req.future(response)
        }

        request2.whenFailure { (error) in
            print(error)
        }

        request2.whenSuccess { (response) in
            print(response)
        }

        return "Hello, world!"
    }

    let password = PlaintextVerifier()
    let basicAuth = MyBasicAuth.basicAuthMiddleware(using: password)

    router.grouped(basicAuth).get("hellow") { req -> Response in
        var http = HTTPResponse()

        if let s = try? req.requireAuthenticated(MyBasicAuth.self) {
            http.body = HTTPBody(string: "\(s.basicUsername) : \(s.basicPassword)")
        } else {
            http.headers.add(
                name: HTTPHeaderName.wwwAuthenticate,
                value: "Basic realm=\"My Website\""
            )

            http.status = .unauthorized
        }

        return Response(http: http, using: req)
    }

    // Example of configuring a controller
    let todoController = TodoController()
    router.get("rur", use: todoController.index)
    router.get("save-fee-type", use: todoController.saveFeeType)
    router.get("save-money-owner", use: todoController.saveMoneyOwner)
    router.get("save-conversion-id", use: todoController.saveConversionId)
    router.get("save-transfer-id", use: todoController.saveTransferId)
    router.get("log", use: todoController.log)
}
