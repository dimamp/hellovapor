//
//  MonthRUR.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

struct Dependency: Codable {
    let rur_to_user: Int
    let usd_to_rur: Int
    let conversion_fee: Int?
}

class MonthRUR: AccountMonth {
    enum CodingKeys: String, CodingKey {
        case rows,
            items,
            input,
            out,
            usd_to_rur_to_dima,
            dimaBalance,
            olesyaBalance
    }

    var rows: [ItemProtocol] {
        return items
    }
    var items: [Item] = []
    var input = MonthRURInput()
    var out = MonthRUROutput()

    var usd_to_rur_to_dima: [Dependency] = []
    
    var dimaBalance: String {
        let total = input.dima_rur.total - out.dima_rur.total
        return String(format: "%.2f", total)
    }
    
    var olesyaBalance: String {
        let total = input.olesya_rur.total
            + input.olesya_other.total
            - out.olesya_rur.total
            - out.eng.total
            - out.elba.total
            - out.tax.total
            - out.olesya_debit.total

        return String(format: "%.2f", total)
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decode([Item].self, forKey: .items)
        input = try values.decode(MonthRURInput.self, forKey: .input)
        out = try values.decode(MonthRUROutput.self, forKey: .out)
        usd_to_rur_to_dima = try values.decode(
            [Dependency].self,
            forKey: .usd_to_rur_to_dima
        )
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(items, forKey: .items)
        try container.encode(input, forKey: .input)
        try container.encode(out, forKey: .out)
        try container.encode(usd_to_rur_to_dima, forKey: .usd_to_rur_to_dima)
        try container.encode(dimaBalance, forKey: .dimaBalance)
        try container.encode(olesyaBalance, forKey: .olesyaBalance)
    }

    init(items: [Item]) {
        self.items = items
    }

    func set(balance: Double, forRow: Int) {
        items[forRow].balance = balance
    }

    func calc(inputBalance: Double, prevMonth: Month?, nextMonth: Month?) {
        input.calc(self, inputBalance: inputBalance, prevMonth: prevMonth, nextMonth: nextMonth)
        out.calc(self, prevMonth: prevMonth, nextMonth: nextMonth)
    }
}
