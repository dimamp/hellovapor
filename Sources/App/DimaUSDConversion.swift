//
//  DimaUSDConversion.swift
//  App
//
//  Created by Dima on 19.12.2019.
//

import Foundation

// На основе истории Skype и т.д.
let knownConversions = [
    USDConversion(year: 2015, month: 6, day: 2, sum: 2700, to: .dima),
    USDConversion(year: 2015, month: 7, day: 1, sum: 2700, to: .dima),
    USDConversion(year: 2015, month: 9, day: 22, sum: 2700, to: .dima),
    USDConversion(year: 2015, month: 10, day: 26, sum: 1000, to: .dima),
    USDConversion(year: 2015, month: 11, day: 12, sum: 1000, to: .dima),

    USDConversion(year: 2016, month: 4, day: 15, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 5, day: 12, sum: 200, to: .dima),
    USDConversion(year: 2016, month: 5, day: 19, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 6, day: 3, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 6, day: 27, sum: 700, to: .dima),
    USDConversion(year: 2016, month: 7, day: 15, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 7, day: 29, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 8, day: 23, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 9, day: 15, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 10, day: 13, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 11, day: 2, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 11, day: 17, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 11, day: 28, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 12, day: 14, sum: 1000, to: .dima),
    USDConversion(year: 2016, month: 12, day: 29, sum: 1000, to: .dima),

    USDConversion(year: 2017, month: 12, day: 22, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 11, day: 30, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 10, day: 26, sum: 500, to: .dima),
    USDConversion(year: 2017, month: 10, day: 6, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 9, day: 15, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 8, day: 3, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 8, day: 25, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 7, day: 20, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 7, day: 27, sum: 300, to: .dima),
    USDConversion(year: 2017, month: 6, day: 27, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 6, day: 9, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 5, day: 17, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 4, day: 24, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 4, day: 12, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 3, day: 31, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 3, day: 22, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 3, day: 9, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 2, day: 28, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 2, day: 14, sum: 1000, to: .dima),
    USDConversion(year: 2017, month: 1, day: 20, sum: 1000, to: .dima),

    USDConversion(year: 2018, month: 9, day: 28, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 9, day: 11, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 8, day: 23, sum: 2000, to: .dima),
    USDConversion(year: 2018, month: 8, day: 22, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 7, day: 31, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 7, day: 18, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 5, day: 22, sum: 2000, to: .dima),
    USDConversion(year: 2018, month: 4, day: 27, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 4, day: 9, sum: 5500, to: .dima),
    USDConversion(year: 2018, month: 3, day: 30, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 3, day: 26, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 3, day: 1, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 2, day: 20, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 2, day: 1, sum: 1000, to: .dima),
    USDConversion(year: 2018, month: 1, day: 12, sum: 1000, to: .dima),

    USDConversion(year: 2019, month: 2, day: 28, sum: 1000, to: .dima),
]

struct USDConversion: Hashable, Codable {
    let year: Int
    let month: Int
    let day: Int
    let sum: Double
    let to: Person
    var usdRate: Double?
    var conversion: Item?
    var transfer: [Item] = []
    var transferComissions: [Item] = []
    var conversionComission: [Item] = []
    var color: String

    var hashValue: Int {
        return Int("\(year)\(month)\(day)" + String(format: "%d", sum))!
    }

    var date: Date? {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy.M.d"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC")
        
        return dateFormat.date(from: "\(year).\(month).\(day)")
    }

    static func == (lhs: USDConversion, rhs: USDConversion) -> Bool {
        return lhs.year == rhs.year
            && lhs.month == rhs.month
            && lhs.day == rhs.day
            && lhs.sum == rhs.sum
            && lhs.to == rhs.to
    }

    init(year: Int, month: Int, day: Int, sum: Double, to: Person) {
        self.year = year
        self.month = month
        self.day = day
        self.sum = sum
        self.to = to
        color = randomColor()
    }

    init(conversion: Item, to: Person) {
        self.to = to
        self.conversion = conversion

        let date = conversion.dateAsDate!
        let c = Calendar.current
        
        year = c.component(.year, from: date)
        month = c.component(.month, from: date)
        day = c.component(.day, from: date)
        usdRate = conversion.getExchangeRate()
        sum = conversion.value! / usdRate!
        color = randomColor()
    }

    func compare(date: Date) -> Bool {
        let c = Calendar.current
        return year == c.component(.year, from: date)
            && month == c.component(.month, from: date)
            && day == c.component(.day, from: date)
    }

    func getItemIds() -> [Int] {
        var ids: [Int] = []

        if let id = conversion?.id {
            ids.append(id)
        }

        transfer.forEach { (item) in ids.append(item.id) }
        transferComissions.forEach { (item) in ids.append(item.id) }
        conversionComission.forEach { (item) in ids.append(item.id) }

        return ids
    }

    func contains(_ id: Int) -> Bool {
        return getItemIds().contains(id)
    }
}
