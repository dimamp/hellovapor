//
//  MonthUSD.swift
//  App
//
//  Created by Dima on 21.12.2019.
//

import Foundation

class MonthUSD: AccountMonth {
    var items: [Item] = []
    var rows: [ItemProtocol] {
        return items
    }
    var input = Value()
    var out_to_rur = Value()
    var out_to_usd = Value()
    var out_to_bank = Value()
    var out_to_agent = Value()
    var out_to_sell = Value()

    var inputBalance: Double = 0
    var balance: Double = 0
    
    init(items: [Item]) {
        self.items = items
    }
    
    func calc(prevMonth: Month?, inputBalance: Double) {
        self.inputBalance = inputBalance

        calcInput(prevMonth)
        calcOutput(prevMonth)

        // баланс
        let prev_balance = prevMonth?.usd?.balance ?? inputBalance
        balance = prev_balance + input.now
            - out_to_rur.now
            - out_to_usd.now
            - out_to_bank.now
            - out_to_agent.now
            - out_to_sell.now
    }

    private func calcInput(_ prevMonth: Month?) {
        // суммарный приход
        input.now = items.reduce(0) { (result, item) -> Double in
            return (item.d_c == "C")
                ? result + item.value!
                : result
        }
        input.prev = prevMonth?.usd?.input.total ?? inputBalance
        input.total = input.prev + input.now
    }

    private func calcOutput(_ prevMonth: Month?) {
        // перевод на рублевый счет ИП
        out_to_rur.now = items.reduce(0) { (result, item) -> Double in
            return (item.d_c == "D" && item.pol_acc == "40802810023070000221")
                ? result + item.value!
                : result
        }

        out_to_rur.prev = prevMonth?.usd?.out_to_rur.total ?? 0
        out_to_rur.total = out_to_rur.prev + out_to_rur.now

        // перевод на долларовую карту Олеси
        out_to_usd.now = items.reduce(0) { (result, item) -> Double in
            return (item.d_c == "D" && item.pol_acc == "40817840408390001145")
                ? result + item.value!
                : result
        }
        out_to_usd.prev = prevMonth?.usd?.out_to_usd.total ?? 0
        out_to_usd.total = out_to_usd.prev + out_to_usd.now

        // комиссия за обслуживание валютного счета ИП
        out_to_bank.now = items.reduce(0) { (result, item) -> Double in
            return (item.d_c == "D" && item.pol_acc == "47423840523070000050")
                ? result + item.value!
                : result
        }
        out_to_bank.prev = prevMonth?.usd?.out_to_bank.total ?? 0
        out_to_bank.total = out_to_bank.prev + out_to_bank.now

        // комиссия за валютный контроль
        out_to_agent.now = items.reduce(0) { (result, item) -> Double in
            return (item.d_c == "D" && (
                item.pol_acc == "70601810423071620303"
                || item.pol_acc == "70601810623072720345"
            ))
                ? result + item.value!
                : result
        }
        out_to_agent.prev = prevMonth?.usd?.out_to_agent.total ?? 0
        out_to_agent.total = out_to_agent.prev + out_to_agent.now

        // продажа валюты
        out_to_sell.now = items.reduce(0) { (result, item) -> Double in
            return (item.d_c == "D" && item.pol_acc == "47408840823070000005")
                ? result + item.value!
                : result
        }
        out_to_sell.prev = prevMonth?.usd?.out_to_sell.total ?? 0
        out_to_sell.total = out_to_sell.prev + out_to_sell.now
    }

    func set(balance: Double, forRow: Int) {
        items[forRow].balance = balance
    }
}
